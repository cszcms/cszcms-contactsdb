<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CSZ CMS
 *
 * An open source content management system
 *
 * Copyright (c) 2016, Astian Foundation.
 *
 * Astian Develop Public License (ADPL)
 * 
 * This Source Code Form is subject to the terms of the Astian Develop Public
 * License, v. 1.0. If a copy of the APL was not distributed with this
 * file, You can obtain one at http://astian.org/about-ADPL
 * 
 * @author	CSKAZA
 * @copyright   Copyright (c) 2016, Astian Foundation.
 * @license	http://astian.org/about-ADPL	ADPL License
 * @link	https://www.cszcms.com
 * @since	Version 1.0.0
 */
class Contactsdb_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function AdminMenuActive($menu_page, $cur_page, $addeditdel = '') {
        /* $addeditdel = 'cat'; //Example: catNew, catEdit, catDel etc. */
        if ($menu_page == $cur_page || ($addeditdel != '' && strpos($cur_page, $addeditdel) !== false)) {
            $active = ' class="active"';
        } else {
            $active = "";
        }
        return $active;
    }

    public function AdminMenu() {
        $cur_page = $this->uri->segment(4);
        $html = '<nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand visible-sm visible-xs">' . $this->lang->line('contactsdb_header') . '</div>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">';
                        $html .= '<li' . $this->AdminMenuActive('index', $cur_page) . '><a href="' . $this->Csz_model->base_link() . '/admin/plugin/contactsdb/index">' . $this->lang->line('contactsdb_menu') . '</a></li>';
                        if ($this->Csz_auth_model->is_group_allowed('Contacts DB', 'backend') !== FALSE) $html .= '<li' . $this->AdminMenuActive('contactIndex', $cur_page, 'contact') . '><a href="' . $this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactIndex">' . $this->lang->line('contactsdb_header') . '</a></li>';
                        if ($this->Csz_auth_model->is_group_allowed('Contacts DB Newsletter', 'backend') !== FALSE) $html .= '<li' . $this->AdminMenuActive('newsletter', $cur_page, 'newsletter') . '><a href="' . $this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletter">' . $this->lang->line('contactsdb_newsletter') . '</a></li>';
                        if ($this->Csz_auth_model->is_group_allowed('Contacts DB', 'backend') !== FALSE) $html .= '<li' . $this->AdminMenuActive('typeIndex', $cur_page, 'type') . '><a href="' . $this->Csz_model->base_link() . '/admin/plugin/contactsdb/typeIndex">' . $this->lang->line('contactsdb_type') . '</a></li>';
                    $html .= '</ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>';
        return $html;
    }
    
    public function contactInsert() {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        ($this->input->post('google_map_active')) ? $google_map_active = $this->input->post('google_map_active', TRUE) : $google_map_active = 0;
        ($this->input->post('unsubscribe')) ? $unsubscribe = $this->input->post('unsubscribe', TRUE) : $unsubscribe = 0;
        $this->db->set('company_name', $this->input->post('company_name', TRUE));
        $this->db->set('contactsdb_type_id', $this->input->post('contactsdb_type_id', TRUE));
        $this->db->set('website', $this->input->post('website', TRUE));
        $this->db->set('contact_person', $this->input->post('contact_person', FALSE));
        $this->db->set('person_position', $this->input->post('person_position', FALSE));
        $this->db->set('email', $this->input->post('email', FALSE));
        $this->db->set('phone', $this->input->post('phone', FALSE));
        $this->db->set('mobile', $this->input->post('mobile', FALSE));
        $this->db->set('address', $this->input->post('address', TRUE));
        $this->db->set('city', $this->input->post('city', TRUE));
        $this->db->set('postcode', $this->input->post('postcode', TRUE));
        $this->db->set('active', $active);
        $this->db->set('contact_note', $this->input->post('contact_note', TRUE));
        $this->db->set('followup_date', $this->input->post('followup_date', TRUE));
        $uploaddir = 'photo/plugin/contactsdb/';
        $file_f = $_FILES['upload_file']['tmp_name'];
        $file_name = $_FILES['upload_file']['name'];
        $upload_file = $this->Csz_admin_model->file_upload($file_f, $file_name, '', $uploaddir, time(), '_1');        
        $this->db->set('upload_file', $upload_file);
        $this->db->set('google_map_active', $google_map_active);
        $this->db->set('google_map_lat', $this->input->post('google_map_lat', TRUE));
        $this->db->set('google_map_lng', $this->input->post('google_map_lng', TRUE));
        $this->db->set('unsubscribe', $unsubscribe);
        $this->db->set('timestamp_create', $this->Csz_model->timeNow(), TRUE);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->insert('contactsdb_data');
    }
    
    public function contactUpdate($id) {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        ($this->input->post('google_map_active')) ? $google_map_active = $this->input->post('google_map_active', TRUE) : $google_map_active = 0;
        ($this->input->post('unsubscribe')) ? $unsubscribe = $this->input->post('unsubscribe', TRUE) : $unsubscribe = 0;
        $this->db->set('company_name', $this->input->post('company_name', TRUE));
        $this->db->set('contactsdb_type_id', $this->input->post('contactsdb_type_id', TRUE));
        $this->db->set('website', $this->input->post('website', TRUE));
        $this->db->set('contact_person', $this->input->post('contact_person', FALSE));
        $this->db->set('person_position', $this->input->post('person_position', FALSE));
        $this->db->set('email', $this->input->post('email', FALSE));
        $this->db->set('phone', $this->input->post('phone', FALSE));
        $this->db->set('mobile', $this->input->post('mobile', FALSE));
        $this->db->set('address', $this->input->post('address', TRUE));
        $this->db->set('city', $this->input->post('city', TRUE));
        $this->db->set('postcode', $this->input->post('postcode', TRUE));
        $this->db->set('active', $active);
        $this->db->set('contact_note', $this->input->post('contact_note', TRUE));
        $this->db->set('followup_date', $this->input->post('followup_date', TRUE));
        $uploaddir = 'photo/plugin/contactsdb/';
        if($this->input->post('del_file')){
            $upload_file = '';
            @unlink($uploaddir.$this->input->post('del_file', TRUE));
        }else{
            $upload_file = $this->input->post('tmp_file', TRUE);
            $file_f = $_FILES['upload_file']['tmp_name'];
            $file_name = $_FILES['upload_file']['name'];
            if($file_name){
                if($upload_file || $upload_file != NULL){
                    @unlink($uploaddir . $upload_file);
                }
                $upload_file = $this->Csz_admin_model->file_upload($file_f, $file_name, '', $uploaddir, time(), '_1');
            }
        }
        $this->db->set('upload_file', $upload_file);
        $this->db->set('google_map_active', $google_map_active);
        $this->db->set('google_map_lat', $this->input->post('google_map_lat', TRUE));
        $this->db->set('google_map_lng', $this->input->post('google_map_lng', TRUE));
        $this->db->set('unsubscribe', $unsubscribe);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->where('contactsdb_data_id', $id);
        $this->db->update('contactsdb_data');
    }
    
    public function typeInsert() {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        $this->db->set('type_name', $this->input->post('type_name', TRUE));
        $this->db->set('active', $active);
        $this->db->set('timestamp_create', $this->Csz_model->timeNow(), TRUE);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->insert('contactsdb_type');
    }
    
    public function typeUpdate($id) {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        $this->db->set('type_name', $this->input->post('type_name', TRUE));
        $this->db->set('active', $active);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->where('contactsdb_type_id', $id);
        $this->db->update('contactsdb_type');
    }
    
    public function newsletterInsert() {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        ($this->input->post('non_active')) ? $non_active = $this->input->post('non_active', TRUE) : $non_active = 0;
        ($this->input->post('email_approve')) ? $email_approve = $this->input->post('email_approve', TRUE) : $email_approve = 0;
        $contactsdb_type_id = $this->input->post('contactsdb_type_id', TRUE);
        $email_test = $this->Csz_model->cleanEmailFormat($this->input->post('email_test', TRUE));
        $email_message = $this->input->post('email_message', FALSE);
        $email_from = $this->Csz_model->cleanEmailFormat($this->input->post('email_from', TRUE));
        $email_reply = $this->Csz_model->cleanEmailFormat($this->input->post('email_reply', TRUE));
        $email_subject = $this->input->post('email_subject', TRUE);
        $select_contactS = $this->input->post('select_contactS');
        $select_contact = '';
        if (isset($select_contactS)) {
            if (count($select_contactS) == 1) {
                $select_contact = $select_contactS[0];
            } else {
                $select_contact = implode(",", $select_contactS);
            }
        }
        $this->db->set('email_from', $email_from);
        $this->db->set('email_reply', $email_reply);
        $this->db->set('email_subject', $email_subject);
        $this->db->set('email_message', $email_message);
        $this->db->set('email_approve', $email_approve);
        $this->db->set('select_contact', $select_contact);
        $this->db->set('contactsdb_type_id', $contactsdb_type_id);
        $this->db->set('active', $active);
        $this->db->set('non_active', $non_active);
        $this->db->set('date_send', $this->input->post('date_send', TRUE));
        $this->db->set('external_include', '0');
        $this->db->set('newsletter_sent', '0');
        $this->db->set('newsletter_complete', '0');
        $this->db->set('timestamp_complete', '0000-00-00 00:00:00');
        $this->db->set('emails_count', '0');
        $this->db->set('timestamp_create', $this->Csz_model->timeNow(), TRUE);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->insert('contactsdb_newsletter');
        $contactsdb_newsletter_id = $this->db->insert_id();
        if($email_approve && $contactsdb_newsletter_id){
            $this->Csz_admin_model->removeData('contactsdb_newslettersent', 'contactsdb_newsletter_id', $contactsdb_newsletter_id);
            $search_arr = " unsubscribe = '0' AND email != '' AND (email LIKE '%@%' AND email LIKE '%.%') ";
            if ($active && !$non_active) {
                $search_arr .= " AND active = '1' ";
            }
            if ($non_active && !$active) {
                $search_arr .= " AND active = '0' ";
            }
            if ($select_contact) {
                $search_arr .= " AND contactsdb_data_id IN ('$select_contact') ";
            }
            if ($contactsdb_type_id) {
                $search_arr .= " AND contactsdb_type_id = '$contactsdb_type_id' ";
            }
            $email = $this->Csz_model->getValueArray('*', 'contactsdb_data', $search_arr, '', 0, 'contactsdb_data_id', 'ASC', 'email');
            if($email !== FALSE){
                $emails_count = 1;
                foreach ($email as $val) {
                    if($val['email']){
                        $email1 = $this->Csz_model->cleanEmailFormat($val['email']);
                        $this->db->set('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                        $this->db->set('email', $email1);
                        $this->db->set('sent_n', '0');
                        $this->db->set('read_n', '0');
                        $this->db->insert('contactsdb_newslettersent');
                        $emails_count++;
                    }
                }
                $this->db->set('emails_count', $emails_count);
                $this->db->where('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                $this->db->update('contactsdb_newsletter');
            }
        }
        if ($email_test) {
            $TableHeader = "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>";
            $TableFooter = "</td></tr><tr><td align=\"center\"><a href=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/unsubscribe?NID=0\">Unsubscribe</a></td></tr></table>";
            $ChkImg = "<div><img src=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/chkNewsletterIMG?NID=0\" width=\"1\" height=\"1\"></div>";
            $email_message = str_replace('\"', '"', $email_message);
            $email_message = str_replace("\'", "'", $email_message);
            # ---- send email - renew
            $message_html = $TableHeader . $ChkImg . $email_message . $TableFooter;
            @$this->Csz_model->sendEmail($email_test, $email_subject, $message_html, $email_from, $email_from, '', $email_reply);
        }
    }
    
    public function newsletterUpdate($contactsdb_newsletter_id) {
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        ($this->input->post('non_active')) ? $non_active = $this->input->post('non_active', TRUE) : $non_active = 0;
        ($this->input->post('email_approve')) ? $email_approve = $this->input->post('email_approve', TRUE) : $email_approve = 0;
        ($this->input->post('external_include')) ? $external_include = $this->input->post('external_include', TRUE) : $external_include = 0;
        $contactsdb_type_id = $this->input->post('contactsdb_type_id', TRUE);
        $email_test = $this->Csz_model->cleanEmailFormat($this->input->post('email_test', TRUE));
        $email_message = $this->input->post('email_message', FALSE);
        $email_from = $this->Csz_model->cleanEmailFormat($this->input->post('email_from', TRUE));
        $email_reply = $this->Csz_model->cleanEmailFormat($this->input->post('email_reply', TRUE));
        $email_subject = $this->input->post('email_subject', TRUE);
        $select_contactS = $this->input->post('select_contactS');
        $select_contact = '';
        if (isset($select_contactS)) {
            if (count($select_contactS) == 1) {
                $select_contact = $select_contactS[0];
            } else {
                $select_contact = implode(",", $select_contactS);
            }
        }
        $this->db->set('email_from', $email_from);
        $this->db->set('email_reply', $email_reply);
        $this->db->set('email_subject', $email_subject);
        $this->db->set('email_message', $email_message);
        $this->db->set('email_approve', $email_approve);
        $this->db->set('select_contact', $select_contact);
        $this->db->set('contactsdb_type_id', $contactsdb_type_id);
        $this->db->set('active', $active);
        $this->db->set('non_active', $non_active);
        $this->db->set('date_send', $this->input->post('date_send', TRUE));
        $this->db->set('external_include', $external_include);
        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
        $this->db->where('contactsdb_newsletter_id', $contactsdb_newsletter_id);
        $this->db->update('contactsdb_newsletter');
        if($email_approve && !$external_include && $contactsdb_newsletter_id){
            $this->Csz_admin_model->removeData('contactsdb_newslettersent', 'contactsdb_newsletter_id', $contactsdb_newsletter_id);
            $search_arr = " unsubscribe = '0' AND email != '' AND (email LIKE '%@%' AND email LIKE '%.%') ";
            if ($active && !$non_active) {
                $search_arr .= " AND active = '1' ";
            }
            if ($non_active && !$active) {
                $search_arr .= " AND active = '0' ";
            }
            if ($select_contact) {
                $search_arr .= " AND contactsdb_data_id IN ('$select_contact') ";
            }
            if ($contactsdb_type_id) {
                $search_arr .= " AND contactsdb_type_id = '$contactsdb_type_id' ";
            }
            $email = $this->Csz_model->getValueArray('*', 'contactsdb_data', $search_arr, '', 0, 'contactsdb_data_id', 'ASC', 'email');
            if($email !== FALSE){
                $emails_count = 1;
                foreach ($email as $val) {
                    if($val['email']){
                        $email1 = $this->Csz_model->cleanEmailFormat($val['email']);
                        $this->db->set('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                        $this->db->set('email', $email1);
                        $this->db->set('sent_n', '0');
                        $this->db->set('read_n', '0');
                        $this->db->insert('contactsdb_newslettersent');
                        $emails_count++;
                    }
                }
                $this->db->set('emails_count', $emails_count);
                $this->db->where('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                $this->db->update('contactsdb_newsletter');
            }
        }
        if ($email_test) {
            $TableHeader = "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>";
            $TableFooter = "</td></tr><tr><td align=\"center\"><a href=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/unsubscribe?NID=0\">Unsubscribe</a></td></tr></table>";
            $ChkImg = "<div><img src=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/chkNewsletterIMG?NID=0\" width=\"1\" height=\"1\"></div>";
            $email_message = str_replace('\"', '"', $email_message);
            $email_message = str_replace("\'", "'", $email_message);
            # ---- send email - renew
            $message_html = $TableHeader . $ChkImg . $email_message . $TableFooter;
            @$this->Csz_model->sendEmail($email_test, $email_subject, $message_html, $email_from, $email_from, '', $email_reply);
        }
    }
    
    public function diff2time($date_a, $date_b){  
        $now_time1=strtotime($date_a);  
        $now_time2=strtotime($date_b); 
        if($now_time2 > $now_time1){
            $time_diff=abs($now_time2-$now_time1);  
        }else{
            $time_diff=abs($now_time1-$now_time2);  
        }
        $time_diff_d=floor($time_diff/(3600*24)); // Day
        $time_diff_h=floor(($time_diff/3600)%24); // Hour
        $time_diff_m=floor(($time_diff%3600)/60); // Min                 
        return $time_diff_d." Days ".$time_diff_h." Hours ".$time_diff_m." Mins";  
    }
    
    public function dateFormat($date){
        return date('d M Y', strtotime($date));
    }
    
    public function getTypeName($contactsdb_type_id){
        $type = $this->Csz_model->getValue('type_name', 'contactsdb_type', 'contactsdb_type_id', $contactsdb_type_id, 1);
        if($type !== FALSE){
            return $type->type_name;
        }else{
            return '-';
        }
    }
}
