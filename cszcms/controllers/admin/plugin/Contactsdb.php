<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contactsdb extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        define('LANG', $this->Csz_admin_model->getLang());
        $this->lang->load('admin', LANG);
        $this->lang->load('plugin/contactsdb', LANG);
        $this->template->set_template('admin');
        $this->load->model('plugin/Contactsdb_model');
        $this->_init();
        admin_helper::plugin_not_active('contactsdb');
    }

    public function _init() {
        $row = $this->Csz_admin_model->load_config();
        $pageURL = $this->Csz_admin_model->getCurPages();
        $this->template->set('core_css', $this->Csz_admin_model->coreCss());
        $this->template->set('core_js', $this->Csz_admin_model->coreJs());
        $this->template->set('title', 'Backend System | ' . $row->site_name);
        $this->template->set('meta_tags', $this->Csz_admin_model->coreMetatags('Backend System for CSZ Content Management'));
        $this->template->set('cur_page', $pageURL);
    }
    
    public function index() {
        admin_helper::is_logged_in($this->session->userdata('admin_email')); /* Check is logged in */
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        if(!$this->uri->segment(4)){
            redirect($this->Csz_model->base_link() . '/'.'admin/plugin/contactsdb/index', 'refresh');
            exit();
        }
        $this->csz_referrer->setIndex('contactsdb'); /* Set index page to redirect after save */
        $this->template->setSub('config', $this->Csz_admin_model->load_config());
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/home'); /* Load view file. Please see in plugin directory structure */
    }

    public function contactIndex() {
        admin_helper::is_logged_in($this->session->userdata('admin_email')); /* Check is logged in */
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        $this->csz_referrer->setIndex('contactsdb'); /* Set index page to redirect after save */
        $this->load->library('pagination');
        $search_arr = ' 1=1 ';
        if($this->input->get('active') == '1'){
            $search_arr.= " AND active = '1' ";
        }else if($this->input->get('active') == 'no'){
            $search_arr.= " AND active = '0' ";
        }
        if($this->input->get('search')){
            $search_arr.= " AND (company_name LIKE '%".$this->input->get('search', TRUE)."%' OR contact_person LIKE '%".$this->input->get('search', TRUE)."%' OR email LIKE '%".$this->input->get('search', TRUE)."%' OR city LIKE '%".$this->input->get('search', TRUE)."%') ";
        }
        if ($this->input->get('followup_date')){
            $search_arr.= " AND followup_date = '" . $this->input->get('followup_date') . "' ";
        }
        if($this->input->get('contactsdb_type_id')){
            $search_arr.= " AND (contactsdb_type_id = '" . $this->input->get('contactsdb_type_id') . "')";
        }
        if($this->input->get('unsubscribe') && !$this->input->get('subscribe')){
            $search_arr.= " AND (unsubscribe = '1')";
        }
        if(!$this->input->get('unsubscribe') && $this->input->get('subscribe')){
            $search_arr.= " AND (unsubscribe = '0')";
        }
        // Pages variable
        $result_per_page = 50;
        $total_row = $this->Csz_model->countData('contactsdb_data', $search_arr);
        $num_link = 10;
        $base_url = $this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactIndex';

        // Pageination config
        $this->Csz_admin_model->pageSetting($base_url,$total_row,$result_per_page,$num_link,5);     
        ($this->uri->segment(5))? $pagination = $this->uri->segment(5) : $pagination = 0;

        //Get users from database
        $this->template->setSub('contactsdb', $this->Csz_admin_model->getIndexData('contactsdb_data', $result_per_page, $pagination, 'timestamp_create', 'desc', $search_arr));
        $this->template->setSub('total_row', $total_row);
        $this->template->setSub('get_type', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
        $this->template->setSub('lastNewsID', $this->Csz_model->getLastID('contactsdb_newsletter', 'contactsdb_newsletter_id', "email_approve != '1'"));
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/contact_index'); /* Load view file. Please see in plugin directory structure */
    }
    
    public function contactNew() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        $config = $this->Csz_admin_model->load_config();
        if(!empty($config->gmaps_key) && $config->gmaps_key != NULL){
            $this->template->setJS('<script type="text/javascript">
                    function initAutocomplete() {
                        var lat_val = '.$config->gmaps_lat.';
                        var lng_val = '.$config->gmaps_lng.';
                        var map = new google.maps.Map(document.getElementById("map"), {
                            center: {lat: lat_val, lng: lng_val},
                            zoom: 8,
                            mapTypeId: "roadmap"
                        });
                        var input = document.getElementById("pac-input");
                        var myLatlng = new google.maps.LatLng(lat_val, lng_val);
                        var searchBox = new google.maps.places.SearchBox(input);
                        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                        map.addListener("bounds_changed", function () {
                            searchBox.setBounds(map.getBounds());
                        });
                        var markers;           
                        markers = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            draggable:true
                        });
                        searchBox.addListener("places_changed", function () {
                            var places = searchBox.getPlaces();
                            if (places.length == 0) {
                                return;
                            }
                            var bounds = new google.maps.LatLngBounds();
                            places.forEach(function (place) {
                                if (!place.geometry) {
                                    console.log("Returned place contains no geometry");
                                    return;
                                }
                                markers.setPosition(place.geometry.location);
                                setLatLngField(place.geometry.location.lat(), place.geometry.location.lng());
                                if (place.geometry.viewport) {
                                    bounds.union(place.geometry.viewport);
                                } else {
                                    bounds.extend(place.geometry.location);
                                }
                            });
                            map.fitBounds(bounds);
                            var listener = google.maps.event.addListener(map, "idle", function () {
                                if (map.getZoom() > 15) map.setZoom(15);
                                google.maps.event.removeListener(listener);
                            });
                        });
                        $("#zoomoutall").click(function () {
                            input.value = "";
                            map.setCenter(myLatlng);
                            map.setZoom(8);
                            markers.setPosition(myLatlng);
                            setLatLngField(lat_val, lng_val); 
                        });
                        markers.addListener("dragend", function(){
                            setLatLngField(markers.position.lat(), markers.position.lng());
                        });
                    }       
                    function setLatLngField(lat, lng){
                        document.getElementById("google_map_lat").value = lat;
                        document.getElementById("google_map_lng").value = lng;
                    }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key='.$config->gmaps_key.'&libraries=places&callback=initAutocomplete" async defer></script>');
        }
        //Load the form helper
        $this->load->helper('form');
        $this->template->setSub('get_type', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
        $this->template->setSub('config', $config);
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/contact_add');
    }
    
    public function contactNewSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('save');
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('company_name', $this->lang->line('contactsdb_company_name'), 'required|is_unique[contactsdb_data.company_name]');
        $this->form_validation->set_rules('contactsdb_type_id', $this->lang->line('contactsdb_type'), 'required');
        if ($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->contactNew();
        } else {
            //Validation passed
            //Add the user
            $this->Contactsdb_model->contactInsert();
            $this->Csz_model->clear_file_cache('contactsdb_getWidget*', TRUE);
            $this->db->cache_delete_all();
            $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function contactEdit() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        //Load the form helper
        $this->load->helper('form');
        if ($this->uri->segment(5)) {
            $config = $this->Csz_admin_model->load_config();
            $contact = $this->Csz_model->getValue('*', 'contactsdb_data', 'contactsdb_data_id', $this->uri->segment(5), 1);
            if (!$contact->google_map_lat || !$contact->google_map_lng) {
                $contact->google_map_lat = $config->gmaps_lat;
                $contact->google_map_lng = $config->gmaps_lng;
            }
            if(!empty($config->gmaps_key) && $config->gmaps_key != NULL){
                $this->template->setJS('<script type="text/javascript">
                        function initAutocomplete() {
                            var lat_val = '.$contact->google_map_lat.';
                            var lng_val = '.$contact->google_map_lng.';
                            var map = new google.maps.Map(document.getElementById("map"), {
                                center: {lat: lat_val, lng: lng_val},
                                zoom: 8,
                                mapTypeId: "roadmap"
                            });
                            var input = document.getElementById("pac-input");
                            var myLatlng = new google.maps.LatLng(lat_val, lng_val);
                            var searchBox = new google.maps.places.SearchBox(input);
                            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                            map.addListener("bounds_changed", function () {
                                searchBox.setBounds(map.getBounds());
                            });
                            var markers;           
                            markers = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                draggable:true
                            });
                            searchBox.addListener("places_changed", function () {
                                var places = searchBox.getPlaces();
                                if (places.length == 0) {
                                    return;
                                }
                                var bounds = new google.maps.LatLngBounds();
                                places.forEach(function (place) {
                                    if (!place.geometry) {
                                        console.log("Returned place contains no geometry");
                                        return;
                                    }
                                    markers.setPosition(place.geometry.location);
                                    setLatLngField(place.geometry.location.lat(), place.geometry.location.lng());
                                    if (place.geometry.viewport) {
                                        bounds.union(place.geometry.viewport);
                                    } else {
                                        bounds.extend(place.geometry.location);
                                    }
                                });
                                map.fitBounds(bounds);
                                var listener = google.maps.event.addListener(map, "idle", function () {
                                    if (map.getZoom() > 15) map.setZoom(15);
                                    google.maps.event.removeListener(listener);
                                });
                            });
                            $("#zoomoutall").click(function () {
                                input.value = "";
                                map.setCenter(myLatlng);
                                map.setZoom(8);
                                markers.setPosition(myLatlng);
                                setLatLngField(lat_val, lng_val); 
                            });
                            markers.addListener("dragend", function(){
                                setLatLngField(markers.position.lat(), markers.position.lng());
                            });
                        }       
                        function setLatLngField(lat, lng){
                            document.getElementById("google_map_lat").value = lat;
                            document.getElementById("google_map_lng").value = lng;
                        }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key='.$config->gmaps_key.'&libraries=places&callback=initAutocomplete" async defer></script>');
            }
            $this->template->setSub('get_type', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
            $this->template->setSub('config', $config);
            $this->template->setSub('contact', $contact);
            //Load the view
            $this->template->loadSub('admin/plugin/contactsdb/contact_edit');
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function contactEditSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('save');
        if ($this->uri->segment(5)) {
            //Load the form validation library
            $this->load->library('form_validation');
            //Set validation rules
            $this->form_validation->set_rules('company_name', $this->lang->line('contactsdb_company_name'), 'required|is_unique[contactsdb_data.company_name.contactsdb_data_id.'.$this->uri->segment(5).']');
            $this->form_validation->set_rules('contactsdb_type_id', $this->lang->line('contactsdb_type'), 'required');
            if ($this->form_validation->run() == FALSE) {
                //Validation failed
                $this->contactEdit();
            } else {
                //Validation passed
                //Add the user
                $this->Contactsdb_model->contactUpdate($this->uri->segment(5));
                $this->Csz_model->clear_file_cache('contactsdb_getWidget*', TRUE);
                $this->db->cache_delete_all();
                $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
                redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
            }
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function contactDelIndex() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('delete');
        $delR = $this->input->post('delR', TRUE);
        $unsubR = $this->input->post('unsubR', TRUE);
        $all_idR = $this->input->post('all_idR', TRUE);
        $lastnewsR = $this->input->post('lastnewsR', TRUE);
        $all_emailR = $this->input->post('all_emailR', TRUE);
        if(isset($delR)){
            foreach ($delR as $value) {
                if ($value) {
                    $contact = $this->Csz_model->getValue('upload_file', 'contactsdb_data', 'contactsdb_data_id', $value, 1);
                    if($contact !== FALSE){
                        $uploaddir = 'photo/plugin/contactsdb/';
                        @unlink($uploaddir . $contact->upload_file);
                    }
                    $this->Csz_admin_model->removeData('contactsdb_data', 'contactsdb_data_id', $value);
                }
            }
        }
        if(isset($all_idR)){
            foreach ($all_idR as $value) {
                if (@$unsubR[$value]) {
                    $this->db->set('unsubscribe', '1');
                    $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
                    $this->db->where('contactsdb_data_id', $value);
                    $this->db->update('contactsdb_data');
                }else{
                    $this->db->set('unsubscribe', '0');
                    $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
                    $this->db->where('contactsdb_data_id', $value);
                    $this->db->update('contactsdb_data');
                }
            }
        }
        $lastNewsID = $this->Csz_model->getLastID('contactsdb_newsletter', 'contactsdb_newsletter_id', "email_approve != '1'");
        if($lastNewsID && isset($all_emailR)){
            foreach ($all_emailR as $id => $email) {
                if($email){
                    $lastNewsSendID = $this->Csz_model->getLastID('contactsdb_newslettersent', 'contactsdb_newslettersent_id', "contactsdb_newsletter_id = '".$lastNewsID."' AND email = '".$email."'");               
                    if (@$lastnewsR[$id]) {
                        if(!$lastNewsSendID){
                            $this->db->set('contactsdb_newsletter_id', $lastNewsID);
                            $this->db->set('email', $email);
                            $this->db->set('sent_n', '0');
                            $this->db->set('read_n', '0');
                            $this->db->insert('contactsdb_newslettersent');
                            /* update email count + 1 */
                            $this->db->set('external_include', '1');
                            $this->db->set('emails_count', 'emails_count + 1', FALSE);
                            $this->db->where('contactsdb_newsletter_id', $lastNewsID);
                            $this->db->update('contactsdb_newsletter');
                        }
                    }else{
                        if($lastNewsSendID){
                            $this->Csz_admin_model->removeData('contactsdb_newslettersent', 'contactsdb_newslettersent_id', $lastNewsSendID);
                            /* update email count - 1 */
                            $this->db->set('external_include', '1');
                            $this->db->set('emails_count', 'emails_count - 1', FALSE);
                            $this->db->where('contactsdb_newsletter_id', $lastNewsID);
                            $this->db->update('contactsdb_newsletter');
                        }
                    }
                }
            }
            $chkNewsSend = $this->Csz_model->countData('contactsdb_newslettersent', "contactsdb_newsletter_id = '".$lastNewsID."'");
            if(!$chkNewsSend){
                $this->db->set('external_include', '0');
                $this->db->set('emails_count', '0');
                $this->db->where('contactsdb_newsletter_id', $lastNewsID);
                $this->db->update('contactsdb_newsletter');
            }
        }
        $this->Csz_model->clear_file_cache('contactsdb_getWidget*', TRUE);
        $this->db->cache_delete_all();
        $this->session->set_flashdata('error_message','<div class="alert alert-success" role="alert">'.$this->lang->line('success_message_alert').'</div>');
        redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
    }
    
    public function export() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        $this->csz_referrer->setIndex('export');
        $this->load->helper('form');
        $this->template->setSub('getType', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
        $this->template->setSub('fields', $this->db->list_fields('contactsdb_data'));
        $this->template->loadSub('admin/plugin/contactsdb/export_index');
    }
    
    public function getCSV() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('save');
        $search_arr = ' 1=1 ';
        if($this->input->get('active') == '1'){
            $search_arr.= " AND active = '1' ";
        }else if($this->input->get('active') == 'no'){
            $search_arr.= " AND active = '0' ";
        }
        if($this->input->get('contactsdb_type_id')){
            $search_arr.= " AND (contactsdb_type_id = '" . $this->input->get('contactsdb_type_id') . "')";
        }
        $this->Csz_admin_model->exportCSV('contactsdb_data', 'contactsdb_data', $this->input->get('fieldS'), $search_arr, $this->input->get('orderby', TRUE), $this->input->get('sort', TRUE));
    }
    
    public function typeIndex() {
        admin_helper::is_logged_in($this->session->userdata('admin_email')); /* Check is logged in */
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        $this->csz_referrer->setIndex('contactsdb'); /* Set index page to redirect after save */
        $this->load->library('pagination');
        $search_arr = ' 1=1 ';
        if($this->input->get('search')){
            $search_arr.= " AND type_name LIKE '%".$this->input->get('search', TRUE)."%' ";
        }
        // Pages variable
        $result_per_page = 20;
        $total_row = $this->Csz_model->countData('contactsdb_type', $search_arr);
        $num_link = 10;
        $base_url = $this->Csz_model->base_link() . '/admin/plugin/contactsdb/homeType';

        // Pageination config
        $this->Csz_admin_model->pageSetting($base_url,$total_row,$result_per_page,$num_link,5);     
        ($this->uri->segment(5))? $pagination = $this->uri->segment(5) : $pagination = 0;

        //Get users from database
        $this->template->setSub('get_type', $this->Csz_admin_model->getIndexData('contactsdb_type', $result_per_page, $pagination, 'contactsdb_type_id', 'asc', $search_arr));
        $this->template->setSub('total_row', $total_row);
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/type_index'); /* Load view file. Please see in plugin directory structure */
    }
    
    public function typeNew() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        //Load the form helper
        $this->load->helper('form');
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/type_add');
    }
    
    public function typeNewSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('save');
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('type_name', $this->lang->line('contactsdb_type_name'), 'required');
        if ($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->typeNew();
        } else {
            //Validation passed
            //Add the user
            $this->Contactsdb_model->typeInsert();
            $this->db->cache_delete_all();
            $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function typeEdit() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        //Load the form helper
        $this->load->helper('form');
        if ($this->uri->segment(5)) {
            $this->template->setSub('type', $this->Csz_model->getValue('*', 'contactsdb_type', 'contactsdb_type_id', $this->uri->segment(5), 1));
            //Load the view
            $this->template->loadSub('admin/plugin/contactsdb/type_edit');
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function typeEditSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('save');
        if ($this->uri->segment(5)) {
            //Load the form validation library
            $this->load->library('form_validation');
            //Set validation rules
            $this->form_validation->set_rules('type_name', $this->lang->line('contactsdb_type_name'), 'required');
            if ($this->form_validation->run() == FALSE) {
                //Validation failed
                $this->typeEdit();
            } else {
                //Validation passed
                //Add the user
                $this->Contactsdb_model->typeUpdate($this->uri->segment(5));
                $this->db->cache_delete_all();
                $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
                redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
            }
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function typeDelIndex() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('delete');
        $delR = $this->input->post('delR');
        if(isset($delR)){
            foreach ($delR as $value) {
                if ($value) {
                    $this->Csz_admin_model->removeData('contactsdb_type', 'contactsdb_type_id', $value);
                }
            }
        }
        $this->db->cache_delete_all();
        $this->session->set_flashdata('error_message','<div class="alert alert-success" role="alert">'.$this->lang->line('success_message_alert').'</div>');
        redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
    }
    
    public function newsletter() {
        admin_helper::is_logged_in($this->session->userdata('admin_email')); /* Check is logged in */
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */ 
        admin_helper::is_allowchk('Contacts DB Newsletter'); /* Check permission */        
        $this->csz_referrer->setIndex('contactsdb'); /* Set index page to redirect after save */
        $this->load->library('pagination');
        $search_arr = ' 1=1 ';
        if($this->input->get('search')){
            $search_arr.= " AND (email_subject LIKE '%".$this->input->get('search', TRUE)."%' OR email_from LIKE '%".$this->input->get('search', TRUE)."%')";
        }
        // Pages variable
        $result_per_page = 50;
        $total_row = $this->Csz_model->countData('contactsdb_newsletter', $search_arr);
        $num_link = 10;
        $base_url = $this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletter';

        // Pageination config
        $this->Csz_admin_model->pageSetting($base_url,$total_row,$result_per_page,$num_link,5);     
        ($this->uri->segment(5))? $pagination = $this->uri->segment(5) : $pagination = 0;

        //Get users from database
        $this->template->setSub('newsletter', $this->Csz_admin_model->getIndexData('contactsdb_newsletter', $result_per_page, $pagination, 'timestamp_create', 'desc', $search_arr));
        $this->template->setSub('total_row', $total_row);
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/newsletter_index'); /* Load view file. Please see in plugin directory structure */
    }
    
    public function newsletterNew() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        admin_helper::is_allowchk('Contacts DB Newsletter');
        //Load the form helper
        $this->load->helper('form');
        $this->template->setSub('get_type', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
        $this->template->setSub('siteconfig', $this->Csz_admin_model->load_config());
        $this->template->setSub('getContact', $this->Csz_model->getValueArray('*', 'contactsdb_data', "company_name != ''", '', 0, 'company_name', 'ASC'));
        //Load the view
        $this->template->loadSub('admin/plugin/contactsdb/newsletter_add');
    }
    
    public function newsletterNewSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        admin_helper::is_allowchk('Contacts DB Newsletter');
        admin_helper::is_allowchk('save');
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('email_from', 'From Email', 'required');
        $this->form_validation->set_rules('email_reply', 'Reply Email', 'required');
        $this->form_validation->set_rules('email_subject', 'Subject', 'required');
        if ($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->newsletterNew();
        } else {
            //Validation passed
            //Add the user
            $this->Contactsdb_model->newsletterInsert();
            $this->db->cache_delete_all();
            $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function newsletterEdit() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('Contacts DB Newsletter');
        //Load the form helper
        $this->load->helper('form');
        if ($this->uri->segment(5)) {
            $this->template->setSub('get_type', $this->Csz_model->getValueArray('*', 'contactsdb_type', 'active', '1', 0));
            $this->template->setSub('siteconfig', $this->Csz_admin_model->load_config());
            $this->template->setSub('getContact', $this->Csz_model->getValueArray('*', 'contactsdb_data', "company_name != ''", '', 0, 'company_name', 'ASC'));
            $this->template->setSub('newsletter', $this->Csz_model->getValue('*', 'contactsdb_newsletter', 'contactsdb_newsletter_id', $this->uri->segment(5), 1));
            //Load the view
            $this->template->loadSub('admin/plugin/contactsdb/newsletter_edit');
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function newsletterEditSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('Contacts DB Newsletter');
        admin_helper::is_allowchk('save');
        if ($this->uri->segment(5)) {
            //Load the form validation library
            $this->load->library('form_validation');
            //Set validation rules
            $this->form_validation->set_rules('email_from', 'From Email', 'required');
            $this->form_validation->set_rules('email_reply', 'Reply Email', 'required');
            $this->form_validation->set_rules('email_subject', 'Subject', 'required');
            if ($this->form_validation->run() == FALSE) {
                //Validation failed
                $this->newsletterEdit();
            } else {
                //Validation passed
                //Add the user
                $this->Contactsdb_model->newsletterUpdate($this->uri->segment(5));
                $this->db->cache_delete_all();
                $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
                redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
            }
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function newsletterView() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB');
        admin_helper::is_allowchk('Contacts DB Newsletter');
        $this->csz_referrer->setIndex('contactsdb_view'); /* Set index page to redirect after save */
        if ($this->uri->segment(5)) {
            $newsletter = $this->Csz_model->getValue('*', 'contactsdb_newsletter', 'contactsdb_newsletter_id', $this->uri->segment(5), 1);
            $join_db = '';
            $join_where = '';
            $join_type = '';
            $group_by = '';
            $order_by = '';
            $sort_by = '';
            $this->load->library('pagination');
            $search_arr = ' 1=1 ';
            if($this->input->get('unsubscribe')){
                $join_db = 'contactsdb_data';
                $join_where = "(contactsdb_data.email = contactsdb_newslettersent.email AND (contactsdb_data.unsubscribe = '".$this->input->get('unsubscribe')."'))";
                $join_type = 'INNER';
		$search_arr.= " AND contactsdb_newslettersent.contactsdb_newsletter_id = '".$this->uri->segment(5)."' AND contactsdb_newslettersent.email LIKE '%".$this->input->get('search')."%' ";
            }else{
                $join_db = 'contactsdb_data';
                $join_where = "(contactsdb_data.email = contactsdb_newslettersent.email)";
                $join_type = 'INNER';
                $search_arr.= " AND contactsdb_newslettersent.contactsdb_newsletter_id = '".$this->uri->segment(5)."' AND contactsdb_newslettersent.email LIKE '%".$this->input->get('search')."%' ";
            }
            if($this->input->get('read_n')){
		$search_arr.= " AND contactsdb_newslettersent.read_n > '0'";
            }
            if($this->input->get('sortsent')){
                $group_by = 'contactsdb_newslettersent.email';
                $order_by = 'contactsdb_newslettersent.sent_n DESC, contactsdb_newslettersent.contactsdb_newslettersent_id ASC';
                $sort_by = '';
            }else{
                $group_by = 'contactsdb_newslettersent.email';
                $order_by = 'contactsdb_newslettersent.contactsdb_newslettersent_id ASC';
                $sort_by = '';
            }
            // Pages variable
            $result_per_page = 100;
            $total_row = $this->Csz_model->countData('contactsdb_newslettersent', $search_arr, $group_by, $order_by, $sort_by, $join_db, $join_where, $join_type);
            $num_link = 10;
            $base_url = $this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletterView/'.$this->uri->segment(5);

            // Pageination config
            $this->Csz_admin_model->pageSetting($base_url,$total_row,$result_per_page,$num_link,6);     
            ($this->uri->segment(6))? $pagination = $this->uri->segment(6) : $pagination = 0;

            //Get users from database
            $this->template->setSub('newsletter', $newsletter);
            $this->template->setSub('count_read', $this->Csz_model->countData('contactsdb_newslettersent', "contactsdb_newsletter_id = '".$this->uri->segment(5)."' AND read_n > '0'"));
            $this->template->setSub('newsSend', $this->Csz_admin_model->getIndexData('contactsdb_newslettersent', $result_per_page, $pagination, $order_by, $sort_by, $search_arr, $group_by, $join_db, $join_where, $join_type));
            $this->template->setSub('total_row', $total_row);
            //Load the view
            $this->template->loadSub('admin/plugin/contactsdb/newsletter_view');
        } else {
            redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
        }
    }
    
    public function newsletterDelIndex() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        admin_helper::is_allowchk('Contacts DB Newsletter');
        admin_helper::is_allowchk('delete');
        $delR = $this->input->post('delR', TRUE);
        if(isset($delR)){
            foreach ($delR as $value) {
                if ($value) {
                    $this->Csz_admin_model->removeData('contactsdb_newslettersent', 'contactsdb_newsletter_id', $value);
                    $this->Csz_admin_model->removeData('contactsdb_newsletter', 'contactsdb_newsletter_id', $value);
                }
            }
        }
        $this->db->cache_delete_all();
        $this->session->set_flashdata('error_message','<div class="alert alert-success" role="alert">'.$this->lang->line('success_message_alert').'</div>');
        redirect($this->csz_referrer->getIndex('contactsdb'), 'refresh');
    }
    
    public function newsletterViewDel() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('Contacts DB'); /* Check permission */
        admin_helper::is_allowchk('Contacts DB Newsletter');
        if($this->uri->segment(5)) {
            $newsletter = $this->Csz_model->getValue('*', 'contactsdb_newsletter', 'contactsdb_newsletter_id', $this->uri->segment(5), 1);
            if($newsletter !== FALSE){
                if($newsletter->email_approve != 1 && $newsletter->newsletter_complete != 1){
                    $delR = $this->input->post('delR', TRUE);
                    if(isset($delR) && $this->input->post('submit_array')){
                        admin_helper::is_allowchk('delete');
                        foreach ($delR as $value) {
                            if ($value) {
                                $this->Csz_admin_model->removeData('contactsdb_newslettersent', 'contactsdb_newslettersent_id', $value);
                                $this->db->set('emails_count', 'emails_count - 1', FALSE);
                                $this->db->where('contactsdb_newsletter_id', $this->uri->segment(5));
                                $this->db->update('contactsdb_newsletter');
                            }
                        }
                        $news_sendcount = $this->Csz_model->countData('contactsdb_newslettersent', "contactsdb_newsletter_id = '".$this->uri->segment(5)."'");
                        if($news_sendcount == 0){
                            $this->db->set('external_include', '0', FALSE);
                            $this->db->set('emails_count', '0', FALSE);
                            $this->db->where('contactsdb_newsletter_id', $this->uri->segment(5));
                            $this->db->update('contactsdb_newsletter');
                        }
                    }
                    $this->db->cache_delete_all();
                    $this->session->set_flashdata('error_message','<div class="alert alert-success" role="alert">'.$this->lang->line('success_message_alert').'</div>');
                }else if($this->input->post('add_newsletter_unread') && $newsletter->newsletter_complete){
                    admin_helper::is_allowchk('save');
                    $news_sendunread = $this->Csz_model->getValue('*', 'contactsdb_newslettersent', "contactsdb_newsletter_id = '".$this->uri->segment(5)."' AND read_n = '0'", '', 0);
                    if($news_sendunread !== FALSE){
                        $this->db->set('email_from', $newsletter->email_from);
                        $this->db->set('email_reply', $newsletter->email_reply);
                        $this->db->set('email_subject', $newsletter->email_subject);
                        $this->db->set('email_message', $newsletter->email_message);
                        $this->db->set('external_include', '1');
                        $this->db->set('newsletter_sent', '0');
                        $this->db->set('newsletter_complete', '0');
                        $this->db->set('timestamp_complete', '0000-00-00 00:00:00');
                        $this->db->set('emails_count', '0');
                        $this->db->set('timestamp_create', $this->Csz_model->timeNow(), TRUE);
                        $this->db->set('timestamp_update', $this->Csz_model->timeNow(), TRUE);
                        $this->db->insert('contactsdb_newsletter');
                        $contactsdb_newsletter_id = $this->db->insert_id();
                        $emails_count = 1;
                        foreach ($news_sendunread as $value) {
                            $email1 = $this->Csz_model->cleanEmailFormat($value->email);
                            $this->db->set('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                            $this->db->set('email', $email1);
                            $this->db->set('sent_n', '0');
                            $this->db->set('read_n', '0');
                            $this->db->insert('contactsdb_newslettersent');
                            $emails_count++;
                        }
                        $this->db->set('emails_count', $emails_count);
                        $this->db->where('contactsdb_newsletter_id', $contactsdb_newsletter_id);
                        $this->db->update('contactsdb_newsletter');
                        
                    }
                    $this->db->cache_delete_all();
                    $this->session->set_flashdata('error_message','<div class="alert alert-success" role="alert">'.$this->lang->line('success_message_alert').'</div>');
                }
            }else{
                $this->session->set_flashdata('error_message','<div class="alert alert-danger" role="alert">'.$this->lang->line('error_message_alert').'</div>');
            }
        }
        redirect($this->csz_referrer->getIndex('contactsdb_view'), 'refresh');
    }
    
    public function chkCompNameAjax() {
        $company_name = $this->input->get('company_name', TRUE);
        if($company_name){
            $chk_CompName = $this->Csz_model->getValue('*', 'contactsdb_data', 'company_name', $company_name, 1);
            if ($chk_CompName !== FALSE) {
                echo '<a class="text-danger" href="'.$this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactEdit/' . $chk_CompName->contactsdb_data_id . '"><b>' . sprintf($this->lang->line('is_unique'), $chk_CompName->company_name) . '</b></a>';
                exit();
            }
        }
    }
    
}