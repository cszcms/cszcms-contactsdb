<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-edit"></span></i> <?php echo $this->lang->line('contactsdb_type_edit') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_type_edit') ?> <a class="btn btn-default btn-sm" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $this->lang->line('btn_back'); ?></a></div>
        <?php echo form_open_multipart($this->Csz_model->base_link() . '/admin/plugin/contactsdb/typeEditSave/' . $this->uri->segment(5)); ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo form_error('type_name', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
                <label for="type_name"><?php echo $this->lang->line('contactsdb_type_name'); ?>*: </label>
                <input type="text" name="type_name" id="type_name" class="form-control" value="<?php echo $type->type_name ?>" required>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <br>
                <div class="form-control-static">
                    <label style="font-weight:normal;"><input type="checkbox" name="active" value="1"<?php echo ($type->active) ? ' checked': '' ?>/> <?php echo $this->lang->line('contactsdb_active'); ?></label>                    
                </div>
            </div>
        </div>
        <br><br>
        <div class="form-actions">
            <?php
            $data = array(
                'name' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-primary',
                'value' => $this->lang->line('btn_save'),
            );
            echo form_submit($data);
            ?> 
            <a class="btn btn-lg" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
        <?php echo form_close(); ?>
        <!-- /widget-content --> 
    </div>
</div>