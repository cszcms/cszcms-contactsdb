<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-envelope"></span></i> <?php echo $this->lang->line('contactsdb_newsletter') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_newsletter') ?> [<?php echo $this->lang->line('id_col_table') ?>:<?php echo $this->uri->segment(5) ?>] <a class="btn btn-default btn-sm" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $this->lang->line('btn_back'); ?></a></div>
        <div class="row">
	    <div class="col-md-12">
	    	<div class="box box-body table-responsive no-padding">
                    <table class="table table-bordered table-hover table-striped">
	    		<tr>
	    		    <td class="text-right" width="15%"><?php echo $this->lang->line('contactsdb_newsletter_from') ?> : </td>
	    		    <td width="35%"><?php echo $newsletter->email_from ?></td>
	    		    <td class="text-right" width="15%"><?php echo $this->lang->line('contactsdb_newsletter_reply') ?> : </td>
	    		    <td width="35%"><?php echo $newsletter->email_reply ?></td>
	    		</tr>
	    		<tr>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_subject') ?> : </td>
	    		    <td><?php echo $newsletter->email_subject ?></td>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_date_send') ?> : </td>
	    		    <td><?php echo ($newsletter->email_approve == '1') ? date('d M Y, h:iA', strtotime($newsletter->timestamp_update)) : date('d M Y, h:iA', strtotime($newsletter->timestamp_create)) ?><?php if($newsletter->date_send != "0000-00-00" && $newsletter->date_send != NULL) echo " <span class=\"remark\">(".$this->lang->line('contactsdb_newsletter_send').": ".$this->Contactsdb_model->dateFormat($newsletter->date_send).")</span>"; ?></td>
	    		</tr>
	    		<tr>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_approve') ?> : </td>
	    		    <td><?php
				    if($newsletter->email_approve == '1')
					echo "<strong style=\"color:green;\">".$this->lang->line('option_yes')."</strong>";
				    else
					echo "<strong style=\"color:red;\">".$this->lang->line('option_no')."</strong>";
				    ?></td>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_complete') ?> : </td>
	    		    <td><?php
				    if($newsletter->newsletter_complete == '1')
					echo "<strong style=\"color:green;\">".$this->lang->line('option_yes')."</strong>";
				    else
					echo "<strong style=\"color:red;\">".$this->lang->line('option_no')."</strong>";
				    ?></td>
	    		</tr>
	    		<tr>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_count_send') ?> : </td>
	    		    <td><?php echo "<strong style=\"color:gray;\">".number_format($newsletter->newsletter_sent)."</strong>"; ?>
	    			<strong> / <?php
					if($newsletter->emails_count)
					    echo number_format($newsletter->emails_count - 1);
					else
					    echo "-";
					?></strong></td>
	    		    <td class="text-right"><?php echo $this->lang->line('contactsdb_newsletter_count_read') ?> : </td>
	    		    <td>
				    <?php echo "<strong style=\"color:gray;\">".number_format($count_read)."</strong>"; ?>
	    			<strong> / <?php
					if($newsletter->newsletter_sent)
					    echo number_format($newsletter->newsletter_sent);
					else
					    echo "-";
					?></strong>
				    <?php
				    if($newsletter->newsletter_sent){
					$count_read_percent = (($count_read / $newsletter->newsletter_sent) * 100);
					if($count_read_percent >= 50)
					    echo "<strong style=\"color:green;\">(".number_format($count_read_percent, 2)."%)</strong>";
					else
					    echo "<strong style=\"color:red;\">(".number_format($count_read_percent, 2)."%)</strong>";
				    }
				    ?>
	    		    </td>
	    		</tr>
	    	    </table>
	    	</div>
	    	<!---- Search Box ---->
                <form action="<?php echo current_url(); ?>" method="get">
                    <div class="control-group">
                        <label class="control-label" for="search"><?php echo $this->lang->line('search'); ?>: <input type="text" name="search" id="search" class="form-control-static" value="<?php echo $this->input->get('search');?>"></label> &nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="unsubscribe" value="<?php echo $this->input->get('unsubscribe', TRUE) ?>">
	    		<input type="hidden" name="read_n" value="<?php echo $this->input->get('read_n', TRUE) ?>">
                        <input type="submit" name="submit" id="submit" class="btn btn-default" value="<?php echo $this->lang->line('search'); ?>">
                    </div>
                </form>
                <br>
	    	<!---- Search Box ---->
	    	<!---- Listing Body ---->
	    	<?php
                    echo "<a href=\"".$this->Csz_model->base_link() . "/admin/plugin/contact_db/newsletterView/".$this->uri->segment(5)."\" style=\"font-size:10px;\">".$this->lang->line('contactsdb_newsletter_view_all')."</a> | ";
                    echo "<a href=\"".$this->Csz_model->base_link() . "/admin/plugin/contact_db/newsletterView/".$this->uri->segment(5)."?read_n=1\" style=\"font-size:10px;\">".$this->lang->line('contactsdb_newsletter_view_read')."</a> | ";
                    echo "<a href=\"".$this->Csz_model->base_link() . "/admin/plugin/contact_db/newsletterView/".$this->uri->segment(5)."?unsubscribe=1\" style=\"font-size:10px;\">".$this->lang->line('contactsdb_newsletter_view_unsub')."</a> | ";
                    echo "<a href=\"".$this->Csz_model->base_link() . "/admin/plugin/contact_db/newsletterView/".$this->uri->segment(5)."?sortsent=1\" style=\"font-size:10px;\">".$this->lang->line('contactsdb_newsletter_double_send')."</a> | ";
		?>
                <br><br>
                <?php echo form_open($this->Csz_model->base_link() . '/admin/plugin/contact_db/newsletterViewDel/' . $this->uri->segment(5)); ?>
                    <div class="box box-body table-responsive no-padding">	    	    
	    		<table class="table table-bordered table-hover table-striped">
	    		    <thead>
                                <tr>
                                    <?php if($newsletter->email_approve != 1 && $newsletter->newsletter_complete != 1){ ?><th width="5%" class="text-center" style="vertical-align:middle;"><label><input id="sel-chkbox-all" type="checkbox"> <?php echo  $this->lang->line('btn_delete') ?></label></th><?php } ?>
                                    <th class="text-center" style="vertical-align:middle;" width="10%"><?php echo $this->lang->line('id_col_table'); ?></th>
                                    <th class="text-left" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_email'); ?></th>
                                    <th class="text-left" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_company_name'); ?></th>
                                    <th class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_type'); ?></th>
                                    <th class="text-center" style="vertical-align:middle;" width="5%"><?php echo $this->lang->line('contactsdb_newsletter_send'); ?></th>
                                    <th class="text-center" style="vertical-align:middle;" width="5%"><?php echo $this->lang->line('contactsdb_newsletter_read'); ?></th>
                                    <?php if($this->input->get('unsubscribe')){ ?>
                                        <th class="text-center" style="vertical-align:middle;" width="15%"><?php echo $this->lang->line('contactsdb_unsubscribe'); ?></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
				<?php if ($newsSend === FALSE) { ?>
                                    <tr>
                                        <td colspan="9" class="text-center"><span class="h6 error"><?php echo $this->lang->line('data_notfound') ?></span></td>
                                    </tr>                           
                                <?php } else {
                                        foreach ($newsSend as $row_ns) { ?>
                                            <tr>
                                                <?php if($newsletter->email_approve != 1 && $newsletter->newsletter_complete != 1){ ?><td class="text-center" style="vertical-align:middle;"><input type="checkbox" name="delR[]" id="delR" class="selall-chkbox" value="<?php echo $row_ns['contactsdb_newslettersent_id'] ?>"></td><?php } ?>
                                                <td class="text-center" style="vertical-align:middle;"><?php echo $row_ns['contactsdb_newslettersent_id'] ?></td>
                                                <td style="vertical-align:middle;"><?php echo $row_ns['email'] ?></td>
                                                <td style="vertical-align:middle;"><a href="<?php echo $this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactEdit/' . $row_ns['contactsdb_data_id'] ?>" style="text-decoration: none;"><?php echo $row_ns['company_name'] ?></a></td>
                                                <td class="text-center" style="vertical-align:middle;"><a href="<?php echo $this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactEdit/' . $row_ns['contactsdb_data_id'] ?>" style="text-decoration: none;"><?php echo $this->Contactsdb_model->getTypeName($row_ns['contactsdb_type_id']) ?></a></td>
                                                <td class="text-center" style="vertical-align:middle;"><?php
                                                    if($row_ns['sent_n'])
                                                        echo "<strong style=\"color:red;\">".$this->lang->line('option_yes')." (" . $row_ns['sent_n'] . ")</strong>";
                                                    else
                                                        echo "<strong>-</strong>";
                                                    ?></td>
                                                <td class="text-center" style="vertical-align:middle;"><?php
                                                    if($row_ns['read_n'])
                                                        echo "<strong style=\"color:red;\">".$this->lang->line('option_yes')." (" . $row_ns['read_n'] . ")</strong>";
                                                    else
                                                        echo "<strong>-</strong>";
                                                    ?></td>
                                                <?php if($this->input->get('unsubscribe')){ ?>
                                                <td class="text-center" style="vertical-align:middle;"><strong style="color:red;"><?php echo $this->lang->line('option_yes') ?></strong></td>
                                                <?php } ?>
                                            </tr>
				    <?php
                                    }
                                }
				?>
                            </tbody>
	    		</table>  	    
                    </div>
                    <?php if($newsletter->email_approve != 1 && $newsletter->newsletter_complete != 1){ ?>
                            <input type="submit" name="submit_array" id="submit_array" value="<?php echo $this->lang->line('btn_save'); ?>" class="btn btn-primary btn-sm" onClick="return confirm('<?php echo $this->lang->line('delete_message'); ?>')">
                    <?php }else if($newsletter->newsletter_complete){ ?>
                            <input type="submit" name="add_newsletter_unread" id="add_newsletter_unread" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('contactsdb_newsletter_add_unread'); ?>" onClick="return confirm('<?php echo $this->lang->line('delete_message'); ?>')">
                    <?php } ?>
                <?php echo  form_close(); ?>
                <br>
                <?php echo $this->pagination->create_links(); ?> <b><?php echo $this->lang->line('total').' '.$total_row.' '.$this->lang->line('records');?></b>
	    	<!---- Listing Body ---->
	    </div>
	</div>
    </div>
</div>