<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-earphone"></span></i> <?php echo $this->lang->line('contactsdb_header') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_header') ?> <a role="button" href="<?php echo $this->Csz_model->base_link()?>/admin/plugin/contactsdb/contactNew" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> <?php echo $this->lang->line('contactsdb_addnew') ?></a>  <a role="button" href="<?php echo $this->Csz_model->base_link()?>/admin/plugin/contactsdb/export" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-export"></span> <?php echo $this->lang->line('export_import_csv_btn') ?></a></div>
        <form action="<?php echo current_url(); ?>" method="get">
            <div class="control-group">
                <label class="control-label" for="search"><?php echo $this->lang->line('search'); ?>: <input type="text" name="search" id="search" class="form-control-static" value="<?php echo $this->input->get('search');?>"></label> &nbsp;&nbsp;&nbsp;                
                <label class="control-label" for="followup_date"><?php echo $this->lang->line('contactsdb_followup_date'); ?>: <input class="form-control-static form-datepicker" type="text" name="followup_date" id="followup_date" value="<?php if ($this->input->get('followup_date') && ($this->input->get('followup_date') != "0000-00-00")) echo $this->input->get('followup_date'); ?>" size="10"></label> &nbsp;&nbsp;
                <label class="control-label" for="active"><?php echo $this->lang->line('contactsdb_active'); ?>: <select name="active" id="active" class="form-control-static">
                        <option value=""><?php echo $this->lang->line('option_all'); ?></option>
                        <option value="1"<?php if ($this->input->get('active') == '1') echo ' selected="selected"'; ?>><?php echo $this->lang->line('contactsdb_active') ?></option>
                        <option value="no"<?php if ($this->input->get('active') == 'no') echo ' selected="selected"'; ?>><?php echo $this->lang->line('contactsdb_nonactive') ?></option>
                    </select></label> &nbsp;&nbsp;
                <label class="control-label" for="contactsdb_type_id"><?php echo $this->lang->line('contactsdb_type'); ?>: <select name="contactsdb_type_id" id="contactsdb_type_id" class="form-control-static">
                        <option value=""><?php echo  $this->lang->line('option_all') ?></option>
                        <?php foreach ($get_type as $value) { 
                            $contacttype[$value['contactsdb_type_id']] = $value['type_name']; ?>
                            <option value="<?php echo $value['contactsdb_type_id'] ?>"<?php if ($this->input->get('contactsdb_type_id') == $value['contactsdb_type_id']) echo ' selected="selected"'; ?>><?php echo $value['type_name'] ?></option>
                        <?php } ?>
                    </select></label> &nbsp;&nbsp;
                    <label class="control-label"><input type="checkbox" name="unsubscribe" value="1" <?php if ($this->input->get('unsubscribe')) echo "checked"; ?> /> <?php echo $this->lang->line('contactsdb_unsubscribe') ?></label> &nbsp;&nbsp;
                    <label class="control-label"><input type="checkbox" name="subscribe" value="1" <?php if ($this->input->get('subscribe')) echo "checked"; ?> /> <?php echo $this->lang->line('contactsdb_subscribe') ?></label> &nbsp;&nbsp;
                <input type="submit" name="submit" id="submit" class="btn btn-default" value="<?php echo $this->lang->line('search'); ?>">
            </div>
        </form>
        <br><br>
        <?php echo  form_open($this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactDelIndex'); ?>
        <div class="box box-body table-responsive no-padding">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th width="8%" class="text-center" style="vertical-align:middle;"><label><input id="sel-chkbox-all" type="checkbox"> <?php echo $this->lang->line('btn_delete') ?></label></th>
                        <th width="8%" class="text-center" style="vertical-align:middle;"><label><input id="sel-chkbox-all2" type="checkbox"> <?php echo $this->lang->line('contactsdb_unsubscribe') ?></label></th>
                        <th width="8%" class="text-center" style="vertical-align:middle;"><label><input id="sel-chkbox-all3" type="checkbox"> <?php echo $this->lang->line('contactsdb_last_newsletter') ?></label></th>
                        <th width="14%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_company_name'); ?></th>
                        <th width="10%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_person'); ?></th>
                        <th width="12%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_email'); ?></th>
                        <th width="10%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_mobile'); ?></th>
                        <th width="10%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_followup_date'); ?></th>
                        <th width="10%" class="text-center" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_type'); ?></th>
                        <th width="6%"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($contactsdb === FALSE) { ?>
                        <tr>
                            <td colspan="8" class="text-center"><span class="h6 error"><?php echo  $this->lang->line('data_notfound') ?></span></td>
                        </tr>                           
                    <?php } else { ?>
                        <?php
                        foreach ($contactsdb as $u) {
                            if(!$u['active']){
                                $inactive = ' style="vertical-align: middle;color:red;text-decoration:line-through;"';
                            }else{
                                $inactive = '';
                            }
                            echo '<tr>';
                            echo '<td class="text-center" style="vertical-align:middle;">
                                    <input type="checkbox" name="delR[]" id="delR" class="selall-chkbox" value="' . $u['contactsdb_data_id'] . '">
                                </td>';
                            echo '<td class="text-center" style="vertical-align:middle;">
                                    <input type="checkbox" name="unsubR[' . $u['contactsdb_data_id'] . ']" id="unsubR" class="selall-chkbox2" value="1" ' . (($u['unsubscribe']) ? "checked" : '') . '>
                                    <input type="hidden" name="all_idR[]" value="' . $u['contactsdb_data_id'] . '" />
                                </td>';
                            $email = $this->Csz_model->cleanEmailFormat($u['email']);
                            $chk_lastNews = $this->Csz_model->countData('contactsdb_newslettersent', "contactsdb_newsletter_id = '".$lastNewsID."' AND email = '".$email."'");
                            ($chk_lastNews != 0) ? $lastcheck = ' checked' : $lastcheck = '';
                            echo '<td class="text-center" style="vertical-align:middle;">
                                    <input type="checkbox" name="lastnewsR[' . $u['contactsdb_data_id'] . ']" id="lastnewsR" class="selall-chkbox3" value="1"'.$lastcheck.'>
                                    <input type="hidden" name="all_emailR[' . $u['contactsdb_data_id'] . ']" value="' . $email . '" />
                                </td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . $u['company_name'] . '</td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . $u['contact_person'] . '</td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . $u['email'] . '</td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . $u['mobile'] . '<br>'.$u['phone'].'</td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . $u['followup_date'] . '</td>';
                            echo '<td'.$inactive.' class="text-center" style="vertical-align:middle;">' . ($u['contactsdb_type_id'] && $u['contactsdb_type_id'] != NULL && $contacttype[$u['contactsdb_type_id']] ? $contacttype[$u['contactsdb_type_id']] : '-') . '</td>';                           
                            echo '<td class="text-center" style="vertical-align: middle;"><a href="'.$this->Csz_model->base_link().'/admin/plugin/contactsdb/contactEdit/' . $u['contactsdb_data_id'] . '" class="btn btn-default btn-sm" role="button"><i class="glyphicon glyphicon-pencil"></i></a></td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <?php
                $data = array(
                    'name' => 'submit',
                    'id' => 'submit',
                    'class' => 'btn btn-primary',
                    'value' => $this->lang->line('btn_delete'),
                    'onclick' => "return confirm('".$this->lang->line('delete_message')."');",
                );
                echo form_submit($data);
                ?>
            </div>
        </div>
        <?php echo  form_close(); ?><br>
        <?php echo $this->pagination->create_links(); ?> <b><?php echo $this->lang->line('total').' '.$total_row.' '.$this->lang->line('records');?></b>
        <!-- /widget-content --> 
    </div>
</div>