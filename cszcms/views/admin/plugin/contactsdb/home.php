<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-dashboard"></span></i> <?php echo $this->lang->line('contactsdb_menu') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12" style="word-wrap:break-word;">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i><span class="glyphicon glyphicon-envelope"></span></i> <?php echo $this->lang->line('contactsdb_newsletter') ?></h3></div>
            <div class="panel-body">
                <h5><b><?php echo $this->lang->line('contactsdb_newsletter_cronjob_command') ?>:</b></h5>
                <pre>wget <?php echo $this->Csz_model->base_link() ?>/plugin/contactsdb/runnewsletter?pkey=<?php echo $this->Csz_admin_model->getPrivateKey() ?> >/dev/null 2>&1</pre>
                <pre>wget <?php echo $this->Csz_model->base_link() ?>/plugin/contactsdb/runreminder?pkey=<?php echo $this->Csz_admin_model->getPrivateKey() ?> >/dev/null 2>&1</pre>
                <span class="remark"><em><?php echo $this->lang->line('contactsdb_newsletter_cronjob_remark') ?></em></span>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i><span class="glyphicon glyphicon-link"></span></i> <?php echo $this->lang->line('contactsdb_header') ?></h3></div>
            <div class="panel-body">
                <h5><b><?php echo $this->lang->line('contactsdb_gmap_xml') ?>:</b></h5>
                <pre><?php echo $this->Csz_model->base_link().'/plugin/contactsdb/getXML' ?></pre>
                <b>OR</b><br>
                <pre><?php echo $this->Csz_model->base_link().'/plugin/contactsdb/getXML/{type_id}' ?></pre>
                <span class="remark"><em><?php echo $this->lang->line('contactsdb_gmap_remark') ?></em></span>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i><span class="glyphicon glyphicon-globe"></span></i> <?php echo $this->lang->line('contactsdb_gmap_code') ?></h3></div>
            <div class="panel-body">
                <span class="remark"><em><?php echo $this->lang->line('contactsdb_gmap_config_remark') ?></em></span><br><br><br>
                <form action="<?php echo current_url(); ?>" method="get">
                    <div class="control-group">
                        <label class="control-label" for="type_id"><?php echo $this->lang->line('contactsdb_type'); ?> {type_id}: <input type="text" name="type_id" id="type_id" class="form-control-static keypress-number" value="<?php echo $this->input->get('type_id');?>"></label> &nbsp;&nbsp;&nbsp;                
                        <input type="submit" name="submit" id="submit" class="btn btn-default" value="<?php echo $this->lang->line('contactsdb_gmap_getcode'); ?>">
                    </div>
                </form>
                <?php
                if($this->input->get('type_id', TRUE)){
                    $downloadUrl = $this->Csz_model->base_link().'/plugin/contactsdb/getXML/' . $this->input->get('type_id', TRUE);
                }else{
                    $downloadUrl = $this->Csz_model->base_link().'/plugin/contactsdb/getXML';
                }
                ?>
                <h5><b><?php echo $this->lang->line('pages_custom_css'); ?></b></h5>
                <pre>
#map { width: 100%; height: 600px; } 
#pac-input { background-color: #fff; font-family: Roboto; font-size: 15px; font-weight: 300; margin-left: 12px; padding: 0 11px 0 13px; text-overflow: ellipsis; width: 300px; height: 40px; margin-top: 10px; border: 1px solid transparent; border-radius: 2px 0 0 2px; box-sizing: border-box; -moz-box-sizing: border-box; outline: none; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3); } 
#pac-input:focus { border-color: #4d90fe; } 
.pac-container { font-family: Roboto; } 
#type-selector { color: #fff; background-color: #4d90fe; padding: 5px 11px 0px 11px; } 
#type-selector label { font-family: Roboto; font-size: 13px; font-weight: 300; } 
#target { width: 345px; }
                </pre>
                <h5><b><?php echo $this->lang->line('pages_custom_js'); ?></b></h5>
                <pre>
var text_bar_html = ""; 
var gmarkers = []; 
function bindInfoWindow(marker, map, infoWindow, html) { 
    google.maps.event.addListener(marker, 'click', function () { 
        infoWindow.setContent(html); 
        infoWindow.open(map, marker); 
        map.setCenter(marker.getPosition()); 
    }); 
    gmarkers.push(marker); 
} 
function downloadUrl(url, callback) { 
    var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest; 
    request.onreadystatechange = function () { 
        if (request.readyState === 4) { 
            request.onreadystatechange = doNothing; 
            callback(request, request.status); 
        }
    }; 
    request.open('GET', url, true); 
    request.send(null); 
} 
function doNothing() { } 
function initAutocomplete() { 
    var lat_val = <?php echo $config->gmaps_lat?>; 
    var lng_val = <?php echo $config->gmaps_lng?>; 
    var map = new google.maps.Map(document.getElementById('map'), { center: {lat: lat_val, lng: lng_val}, zoom: 7, mapTypeId: 'roadmap' }); 
    var input = document.getElementById('pac-input'); 
    $('#zoomoutall').click(function () { 
        map.setCenter(new google.maps.LatLng(lat_val, lng_val)); 
        map.setZoom(7); 
        infoWindow.close(); 
        input.value = ''; 
    }); 
    var infoWindow = new google.maps.InfoWindow; 
    downloadUrl("<?php echo $downloadUrl ?>", function (data) { 
        var link_url; 
        var xml = data.responseXML; 
        var markers = xml.documentElement.getElementsByTagName("marker"); 
        for (var i = 0; i < markers.length; i++) { 
            var company_name = markers[i].getAttribute("company_name"); 
            var address = markers[i].getAttribute("address"); 
            var city = markers[i].getAttribute("city"); 
            var postcode = markers[i].getAttribute("postcode"); 
            var phone = markers[i].getAttribute("phone"); 
            var url = markers[i].getAttribute("url");
            var point = new google.maps.LatLng( parseFloat(markers[i].getAttribute("lat")), parseFloat(markers[i].getAttribute("lng"))); 
            if(url){ 
                link_url = "&lt;a href=\"" + url + "\" target=\"_blank\">&lt;b>" + company_name + "&lt;/b>&lt;/a>"; 
            }else{ 
                link_url = "&lt;b>" + company_name + "&lt;/b>"; 
            } 
            var html = link_url + "&lt;br>" + address + "&lt;br>" + city + " " + postcode + "&lt;br>Tel. " + phone;
            var marker = new google.maps.Marker({ map: map, position: point, title: company_name }); 
            bindInfoWindow(marker, map, infoWindow, html); 
        } 
    }); 
    /* Create the search box and link it to the UI element.*/ 
    var searchBox = new google.maps.places.SearchBox(input); 
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input); 
    /* Bias the SearchBox results towards current map's viewport.*/ 
    map.addListener('bounds_changed', function () { searchBox.setBounds(map.getBounds()); }); 
    /* Listen for the event fired when the user selects a prediction and retrieve*/ 
    /* more details for that place.*/ 
    searchBox.addListener('places_changed', function () { 
        var places = searchBox.getPlaces(); 
        if (places.length == 0) { return; } 
        /* For each place.*/ 
        var bounds = new google.maps.LatLngBounds(); 
        places.forEach(function (place) { 
            if (!place.geometry) { 
                console.log("Returned place contains no geometry"); 
                return; 
            } 
            if (place.geometry.viewport) { 
                /* Only geocodes have viewport.*/ 
                bounds.union(place.geometry.viewport); 
            } else { 
                bounds.extend(place.geometry.location); 
            } 
        }); 
        map.fitBounds(bounds); 
        var listener = google.maps.event.addListener(map, "idle", function() { 
            if (map.getZoom() > 15) map.setZoom(15); 
            google.maps.event.removeListener(listener); 
        }); 
    }); 
} 
&lt;script src="https://maps.googleapis.com/maps/api/js?key=<?= $config->gmaps_key ?>&libraries=places&callback=initAutocomplete" async defer>
                </pre>
                <h5><b><?php echo $this->lang->line('pages_content'); ?></b></h5>
                <pre>
&lt;div class="container">
&lt;div class="row">
&lt;div class="col-md-12">&lt;br>
&lt;h5>&lt;strong>Please type in your search on the map below:&lt;/strong>&lt;/h5>
&lt;input id="pac-input" placeholder="Search Box" type="text">
&lt;div id="map">&lt;br>&lt;/div>
&lt;br>&lt;center>&lt;input name="zoomoutall" id="zoomoutall" class="btn btn-primary btn-lg" value="Map Reset" type="button">&lt;/center>&lt;br>&lt;/div>
&lt;/div>
&lt;/div>
                </pre>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->