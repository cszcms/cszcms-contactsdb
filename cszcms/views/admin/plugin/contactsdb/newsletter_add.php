<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-edit"></span></i> <?php echo $this->lang->line('contactsdb_newsletter_addnew') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_newsletter_addnew') ?> <a class="btn btn-default btn-sm" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $this->lang->line('btn_back'); ?></a></div>
        <?php echo form_open_multipart($this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletterNewSave'); ?>
        <div class="row">
            <div class="col-md-8">             
                <label><?php echo $this->lang->line('contactsdb_newsletter_from') ?> :<span class="remark"> * </span></label>
                <input type="email" name="email_from" class="form-control" value="" size="50" required>
                <label><?php echo $this->lang->line('contactsdb_newsletter_reply') ?> :<span class="remark"> * </span></label>
                <input type="email" name="email_reply" class="form-control" value="" size="50" required>
                <label><?php echo $this->lang->line('contactsdb_newsletter_subject') ?> :<span class="remark"> * </span></label>        
                <input type="text" name="email_subject" class="form-control" value="" size="50" required>               
                <label><?php echo $this->lang->line('contactsdb_newsletter_message') ?> : </label>
                <?php
                $default_body = '<center><table style="border-color:#006; border-width:5px;" width="550" border="5" cellspacing="5" cellpadding="5"><tr><td><div style="text-align:center;"><a href="'. BASE_URL .'" target="_blank">' . (($siteconfig->site_logo && $siteconfig->site_logo != NULL) ? '<img src="' . base_url().'photo/logo/'. $siteconfig->site_logo .'" style="margin-top:18px;">' : '<h1>' . $siteconfig->site_name . '</h1>') . '</a><hr></div><div style="padding:10px;"><h2>HELLO, WORLD!</h2><p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p><p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p></div></td></tr></table></center>';
                ?>
                <textarea name="email_message" id="email_message" rows="20" class="form-control body-tinymce"><?php echo $default_body ?></textarea>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><b><?php echo $this->lang->line('contactsdb_newsletter_sentto') ?>:</b></div>
                    <div class="panel-body">
                        <em class="remark"><?php echo $this->lang->line('contactsdb_newsletter_sendall_remark') ?></em><br>
                        <label for="select_contact"><?php echo $this->lang->line('contactsdb_newsletter_select_contact') ?>: </label><br>
                        <?php
                        $att = 'data-placeholder="'.$this->lang->line('contactsdb_newsletter_select_contact').':" id="select_contactS" class="form-control select2" multiple="multiple"';
                        $data = array();
                        if (!empty($getContact)) {
                            foreach ($getContact as $value) {
                                $data[$value['contactsdb_data_id']] = $value['company_name'];
                            }
                        }
                        echo form_dropdown('select_contactS[]', $data, '', $att);
                        ?><br>
                        <label for="contactsdb_type_id"><?php echo $this->lang->line('contactsdb_newsletter_contactsdb_type_id') ?>: </label>
                        <?php
                        $att = 'id="contactsdb_type_id" class="form-control"';
                        $data = array();
                        $data[''] = $this->lang->line('option_choose');
                        if (!empty($get_type)) {
                            foreach ($get_type as $value) {
                                $data[$value['contactsdb_type_id']] = $value['type_name'];
                            }
                        }
                        echo form_dropdown('contactsdb_type_id', $data, '', $att);
                        ?>
                        <div class="form-control-static">
                            <label style="font-weight:normal;"><input type="checkbox" name="active" value="1" /> <b><?php echo $this->lang->line('contactsdb_active') ?></b></label> &nbsp;&nbsp;
                            <label style="font-weight:normal;"><input type="checkbox" name="non_active" value="1" /> <b><?php echo $this->lang->line('contactsdb_nonactive') ?></b></label><br> 
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading"><b><?php echo $this->lang->line('contactsdb_newsletter_email_test') ?>:</b></div>
                    <div class="panel-body">
                        <em class="remark"><?php echo $this->lang->line('contactsdb_newsletter_email_test_remark') ?></em><br>
                        <input type="text" name="email_test" id="email_test" class="form-control" value="" size="50">
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading"><b><?php echo $this->lang->line('contactsdb_newsletter_approve') ?>: </b></div>
                    <div class="panel-body">
                        <label><input type="checkbox" name="email_approve" id="email_approve" value="1" /> <?php echo $this->lang->line('contactsdb_newsletter_approve') ?> <em class="remark"><?php echo $this->lang->line('contactsdb_newsletter_approve_remark') ?> </em> <em class="remark"> 
                                <input type="text" name="date_send" id="date_send" class="form-datepicker" placeholder="<?php echo $this->lang->line('contactsdb_newsletter_date_send') ?>" value="" size="10" />
                                <?php echo $this->lang->line('contactsdb_newsletter_date_send_remark') ?></em></label>
                    </div>
                </div>                            
            </div>
        </div>
        <br><br>
        <div class="form-actions">
            <?php
            $data = array(
                'name' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-primary',
                'value' => $this->lang->line('btn_save'),
            );
            echo form_submit($data);
            ?> 
            <a class="btn btn-lg" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
        <?php echo form_close(); ?>
        <!-- /widget-content --> 
    </div>
</div>
