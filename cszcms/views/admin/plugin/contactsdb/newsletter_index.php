<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-envelope"></span></i> <?php echo $this->lang->line('contactsdb_newsletter') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_newsletter') ?> <a role="button" href="<?php echo $this->Csz_model->base_link()?>/admin/plugin/contactsdb/newsletterNew" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> <?php echo $this->lang->line('contactsdb_newsletter_addnew') ?></a></div>
        <form action="<?php echo current_url(); ?>" method="get">
            <div class="control-group">
                <label class="control-label" for="search"><?php echo $this->lang->line('search'); ?>: <input type="text" name="search" id="search" class="form-control-static" value="<?php echo $this->input->get('search');?>"></label> &nbsp;&nbsp;&nbsp;               
                <input type="submit" name="submit" id="submit" class="btn btn-default" value="<?php echo $this->lang->line('search'); ?>">
            </div>
        </form>
        <br><br>
        <?php echo  form_open($this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletterDelIndex'); ?>
        <div class="box box-body table-responsive no-padding">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th width="8%" class="text-center" style="vertical-align:middle;"><label><input id="sel-chkbox-all" type="checkbox"> <?php echo  $this->lang->line('btn_delete') ?></label></th>
                        <th width="20%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_subject') ?></th>
                        <th width="16%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_from') ?></th>
                        <th class="text-center" width="5%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_approve') ?></th>
                        <th class="text-center" width="5%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_complete') ?></th>
                        <th class="text-right" width="6%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_send'); ?></th>
                        <th class="text-center" width="11%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_date_send') ?></th>
                        <th class="text-center" width="6%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_complete_date') ?></th>
                        <th class="text-center" width="10%" style="vertical-align:middle;"><?php echo $this->lang->line('contactsdb_newsletter_period') ?></th>
                        <th width="10%" class="text-center" style="vertical-align:middle;"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($newsletter === FALSE) { ?>
                        <tr>
                            <td colspan="11" class="text-center"><span class="h6 error"><?php echo  $this->lang->line('data_notfound') ?></span></td>
                        </tr>                           
                    <?php } else { ?>
                        <?php
                        foreach ($newsletter as $row) { ?>
                            <tr>
                                <td class="text-center" style="vertical-align:middle;">
                                    <input type="checkbox" name="delR[]" id="delR" class="selall-chkbox" value="<?php echo $row['contactsdb_newsletter_id'] ?>">
                                </td>
                                <td style="vertical-align:middle;"><?php echo $row['email_subject']?></td>
                                <td style="vertical-align:middle;"><?php echo $row['email_from']?></td>
                                <td align="center" style="vertical-align:middle;"><?php if($row['email_approve']) echo "<strong style=\"color:green;\">".$this->lang->line('option_yes')."</strong>"; else echo "<strong style=\"color:red;\">".$this->lang->line('option_no')."</strong>";?></td>
                                <td align="center" style="vertical-align:middle;"><?php if($row['newsletter_complete']) echo "<strong style=\"color:green;\">".$this->lang->line('option_yes')."</strong>"; else echo "<strong style=\"color:red;\">".$this->lang->line('option_no')."</strong>";?></td>
                                <td align="right" style="vertical-align:middle;">
                                <?php
                                        if($row['email_approve'] && $row['newsletter_complete']){
                                            echo "<strong style=\"color:green;\">".number_format($row['newsletter_sent'])." / ".number_format($row['emails_count']-1)."</strong>";
                                        }else{
                                            echo "<strong style=\"color:red;\">".number_format($row['newsletter_sent'])." / ".number_format($row['emails_count']-1)."</strong>";
                                        }
                                ?>
                                </td>
                                <td align="center" style="vertical-align:middle;"><?php echo ($row['email_approve'] == '1') ? date('d M Y, h:iA', strtotime($row['timestamp_update'])) : date('d M Y, h:iA', strtotime($row['timestamp_create'])) ?><?php if($row['date_send'] != "0000-00-00" && $row['date_send'] != NULL) echo "<br/><span class=\"remark\">(".$this->lang->line('contactsdb_newsletter_send').": ".$this->Contactsdb_model->dateFormat($row['date_send']).")</span>";?></td>
                                <td align="center" style="vertical-align:middle;"><?php echo ($row['timestamp_complete'] != "0000-00-00 00:00:00") ? $this->Contactsdb_model->dateFormat($row['timestamp_complete']) : "-" ?></td>
                                <td align="center" style="vertical-align:middle;">
                                    <?php
                                    $period_time = '';
                                    if($row['date_send'] != "0000-00-00" && $row['email_approve'] == '1'){
                                        $period_time = $this->Contactsdb_model->diff2time($row['date_send']." 00:00:00", $row['timestamp_complete']);
                                    }else if($row['email_approve'] == '1' && $row['date_send'] == "0000-00-00"){
                                        $period_time = $this->Contactsdb_model->diff2time($row['timestamp_update'], $row['timestamp_complete']);
                                    } ?>
                                    <?php echo ($row['timestamp_complete'] != '0000-00-00 00:00:00') ? $period_time : "-"?>
                                </td>                            
                                <td class="text-center" style="vertical-align:middle;font-size:16px;">
                                    <a href="<?php echo $this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletterEdit/' . $row['contactsdb_newsletter_id'] ?>" style="text-decoration: none;"><span class="glyphicon glyphicon-pencil"></span></a> &nbsp; <a href="<?php echo $this->Csz_model->base_link() . '/admin/plugin/contactsdb/newsletterView/' . $row['contactsdb_newsletter_id'] ?>" style="text-decoration: none;"><span class="glyphicon glyphicon-list-alt"></span></a></td>
                            </tr>
                        <?php }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <?php
                $data = array(
                    'name' => 'submit',
                    'id' => 'submit',
                    'class' => 'btn btn-primary',
                    'value' => $this->lang->line('btn_save'),
                    'onclick' => "return confirm('".$this->lang->line('delete_message')."');",
                );
                echo form_submit($data);
                ?>
            </div>
        </div>
        <?php echo  form_close(); ?><br>
        <?php echo $this->pagination->create_links(); ?> <b><?php echo $this->lang->line('total').' '.$total_row.' '.$this->lang->line('records');?></b>
        <!-- /widget-content --> 
    </div>
</div>