<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Admin Menu -->
        <?php echo $this->Contactsdb_model->AdminMenu() ?>
        <!-- End Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-edit"></span></i> <?php echo $this->lang->line('contactsdb_addnew') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo $this->lang->line('contactsdb_addnew') ?> <a class="btn btn-default btn-sm" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $this->lang->line('btn_back'); ?></a></div>
        <?php echo form_open_multipart($this->Csz_model->base_link() . '/admin/plugin/contactsdb/contactNewSave', ' name="company"'); ?>
        <div class="row">
            <div class="col-md-6">
                <?php echo form_error('company_name', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
                <label for="company_name"><?php echo $this->lang->line('contactsdb_company_name'); ?>*: </label> <span id="compnameres"></span>
                <input onchange="chkCompName()" onmousedown="chkCompName()" onmouseout="chkCompName()" type="text" name="company_name" id="company_name" class="form-control" value="" required>
            </div>
            <div class="col-md-3">
                <?php echo form_error('contactsdb_type_id', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
                <label for="contactsdb_type_id"><?php echo $this->lang->line('contactsdb_type'); ?>*: </label>
                <?php
                $att = 'id="contactsdb_type_id" class="form-control" required';
                $data = array();
                $data[''] = $this->lang->line('option_choose');
                if (!empty($get_type)) {
                    foreach ($get_type as $value) {
                        $data[$value['contactsdb_type_id']] = $value['type_name'];
                    }
                }
                echo form_dropdown('contactsdb_type_id', $data, '', $att);
                ?>
            </div>
            <div class="col-md-3">
                <label for="website"><?php echo $this->lang->line('contactsdb_website'); ?>: </label>
                <input type="text" name="website" id="website" class="form-control">
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="contact_person"><?php echo $this->lang->line('contactsdb_person'); ?>: </label>
                <input type="text" name="contact_person" id="contact_person" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="person_position"><?php echo $this->lang->line('contactsdb_position'); ?>: </label>
                <input type="text" name="person_position" id="person_position" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="email"><?php echo $this->lang->line('contactsdb_email'); ?>: </label>
                <input type="email" name="email" id="email" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="phone"><?php echo $this->lang->line('contactsdb_phone'); ?>: </label>
                <input type="text" name="phone" id="phone" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="mobile"><?php echo $this->lang->line('contactsdb_mobile'); ?>: </label>
                <input type="text" name="mobile" id="mobile" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="address"><?php echo $this->lang->line('contactsdb_address'); ?>: </label>
                <textarea name="address" id="address" rows="2" class="form-control"></textarea>
            </div>
            <div class="col-md-4">
                <label for="city"><?php echo $this->lang->line('contactsdb_city'); ?>: </label>
                <input type="text" name="city" id="city" class="form-control">
            </div>
            <div class="col-md-4">
                <label for="postcode"><?php echo $this->lang->line('contactsdb_postcode'); ?>: </label>
                <input type="text" name="postcode" id="postcode" maxlength="10" class="form-control keypress-number">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <label for="contact_note"><?php echo $this->lang->line('contactsdb_contact_note'); ?>: </label>
                <textarea name="contact_note" id="contact_note" rows="6" class="form-control"></textarea>
            </div>
            <div class="col-md-4">
                <label for="followup_date"><?php echo $this->lang->line('contactsdb_followup_date'); ?>: </label>
                <input type="text" name="followup_date" id="followup_date" class="form-control form-datepicker">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <br>
                <div class="form-control-static">
                    <label style="font-weight:normal;"><input type="checkbox" name="active" value="1"/> <?php echo $this->lang->line('contactsdb_active'); ?></label>                    
                </div>
                <br><label for="upload_file"><?php echo $this->lang->line('contactsdb_upload_file'); ?>: </label>   
                <input type="file" name="upload_file" id="upload_file" class="form-control-static">
                <br>
                <div class="form-control-static">
                    <label style="font-weight:normal;"><input type="checkbox" name="unsubscribe" value="1" /> <?php echo $this->lang->line('contactsdb_unsubscribe') ?></label>                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <br>
                <div class="form-control-static">
                    <label style="font-weight:normal;"><input type="checkbox" name="google_map_active" value="1"/> <?php echo $this->lang->line('contactsdb_map_enable'); ?></label><br>
                </div>
                <style>
                    #map {
                        width: 100%;
                        height: 600px;
                    }
                    #pac-input {
                        background-color: #fff;
                        font-family: Roboto;
                        font-size: 15px;
                        font-weight: 300;
                        margin-left: 12px;
                        padding: 0 11px 0 13px;
                        text-overflow: ellipsis;
                        width: 300px;
                        height: 33px;
                        margin-top: 10px;
                        border: 1px solid transparent;
                        border-radius: 2px 0 0 2px;
                        box-sizing: border-box;
                        -moz-box-sizing: border-box;
                        outline: none;
                        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                    }
                    #pac-input:focus {
                        border-color: #4d90fe;
                    }
                    .pac-container {
                        font-family: Roboto;
                    }
                    #type-selector {
                        color: #fff;
                        background-color: #4d90fe;
                        padding: 5px 11px 0px 11px;
                    }
                    #type-selector label {
                        font-family: Roboto;
                        font-size: 13px;
                        font-weight: 300;
                    }
                    #target {
                        width: 345px;
                    }
                </style>            
                <input id="pac-input" type="text" placeholder="<?php echo $this->lang->line('contactsdb_map_search_box'); ?>">
                <div id="map"></div>
                <br>
                <label><?php echo $this->lang->line('contactsdb_map_drag_cursor'); ?>.</label> <input type="button" name="zoomoutall" id="zoomoutall" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('contactsdb_map_reset'); ?>"/><br>
                <label>Lat: </label>
                <input type="text" name="google_map_lat" id="google_map_lat" value="<?php echo $config->gmaps_lat?>" class="form-control"> 
                <label>Lng: </label>
                <input type="text" name="google_map_lng" id="google_map_lng" value="<?php echo $config->gmaps_lng?>" class="form-control">
            </div>
        </div>
        <br><br>
        <div class="form-actions">
            <?php
            $data = array(
                'name' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-primary',
                'value' => $this->lang->line('btn_save'),
            );
            echo form_submit($data);
            ?> 
            <a class="btn btn-lg" href="<?php echo $this->csz_referrer->getIndex('contactsdb'); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
        <?php echo form_close(); ?>
        <!-- /widget-content --> 
    </div>
</div>
<script type="text/javascript">
    function chkCompName() {
        if(document.company.company_name.value != ''){
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("compnameres").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "<?php echo $this->Csz_model->base_link() . '/' ?>admin/plugin/contactsdb/chkCompNameAjax?company_name=" + document.company.company_name.value, true);
            xhttp.send();
        }
    }
</script>