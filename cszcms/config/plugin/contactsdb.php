<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* For your plugin config.
 * Importnt! Please Don't change the config item index name (for systems config)
 * 
 * For load plugin config:
 * $this->Csz_model->getPluginConfig('plugin_config_filename', 'item index name');
 * Ex. $this->Csz_model->getPluginConfig('article', 'plugin_name');
 * 
 */

/* Start System Config (Important) */
/* General Config */
$plugin_config['plugin_name']  = 'Contacts DB';
$plugin_config['plugin_urlrewrite']  = 'contactsdb'; /* Please don't have any blank space */
$plugin_config['plugin_author']  = 'CSKAZA'; /* For your name */
$plugin_config['plugin_version']   = '1.0.5';
$plugin_config['plugin_description']   = 'Contact Database System and Newsletter System'; /* For your plugin description */

/* for menu inside member zone. If not have please blank. 
 * Example: $plugin_config['plugin_member_menu'] = 'link_name';
 * The link automatic to {base_url}/plugin/{your_plugin_urlrewrite}
 * plugin_menu_permission_name is permission name from user_perms table on DB
 */
$plugin_config['plugin_member_menu'] = '';
$plugin_config['plugin_menu_permission_name'] = '';

/* Database Config */
$plugin_config['plugin_db_table']   = array(
    'contactsdb_data',
    'contactsdb_type',
    'contact_newsletter',
    'contact_newslettersent',
); /* Please input all your pludin db table name */

/* Sitemap Generater Config (for content view page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_viewtable']   = '';
/* for sitemap sql extra condition for this view table. If not have please blank. */
$plugin_config['plugin_sqlextra_condition']   = "";

/* Sitemap Generater Config (for content category page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_cattable']   = '';
/* for sitemap sql extra condition for this category table. If not have please blank. */
$plugin_config['plugin_sqlextra_catcondition']   = "";

/* All your plugin file path 
 * For directory please put / into the end of path.
 * Filename or Directory name is case sensitive.
 */
$plugin_config['plugin_file_path']   = array(
    FCPATH . '/photo/plugin/contactsdb/',
    FCPATH . '/cszcms/config/plugin/contactsdb.php',
    FCPATH . '/cszcms/controllers/admin/plugin/Contactsdb.php',
    FCPATH . '/cszcms/models/plugin/Contactsdb_model.php',
    FCPATH . '/cszcms/modules/plugin/controllers/Contactsdb.php',
    FCPATH . '/cszcms/views/admin/plugin/contactsdb/',
);

/* Plugin widget Config (for content view page only) 
 * If don't have the widget for your plugin. Please blank.
 */
$plugin_config['plugin_widget_viewtable']   = '';
/* for sql extra condition for this view table. If not have please blank. */
$plugin_config['plugin_widget_condition']   = "";
/* for select the field from the database with array('field_name'). If not have please blank. */
$plugin_config['plugin_widget_sel_field']   = array();
/* for sql table join with view table when have view ID. If not have please blank. */
$plugin_config['plugin_widget_othertable']   = '';
$plugin_config['plugin_widget_othertable_idkey']   = '';
$plugin_config['plugin_widget_othertable_condition']   = "";
/* for select the field from the table join with array('field_name'). If not have please blank. */
$plugin_config['plugin_widget_othertable_selfield']   = array();

/* End System Config (Important) */

/* Custom config (For your plugin config)
 * Please add your config after this section
 */
$plugin_config['backend_startup'] = '';
$plugin_config['frontend_startup'] = '';