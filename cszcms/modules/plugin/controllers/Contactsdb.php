<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contactsdb extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        member_helper::plugin_not_active('contactsdb');
    }
    
    public function index() {
        redirect(base_url(), 'refresh');
    }
    
    public function getXML() {
        $config = $this->Csz_model->load_config();
        $this->db->cache_on();
        $this->load->driver('cache', array('adapter' => 'file'));
        if (!$this->cache->get('contactsdb_getWidget_'.$this->uri->segment(4))) {
            // For All Contacts Map
            $this->load->library('Xml_writer');
            // Initiate class
            $xml = new Xml_writer;
            $xml->setRootName('markers');
            $xml->initiate();
            // Start Main branch      
            // Get all contact map
            $this->db->cache_on();
            if($this->uri->segment(4)){
                $search_sql = "active = '1' AND contactsdb_type_id = '".$this->uri->segment(4)."' AND google_map_active = '1' AND (google_map_lat != '' AND google_map_lng != '')";
            }else{
                $search_sql = "active = '1' AND google_map_active = '1' AND (google_map_lat != '' AND google_map_lng != '')";
            }
            $contact = $this->Csz_model->getValueArray('*', 'contactsdb_data', $search_sql, '', 0, 'timestamp_create', 'DESC');
            if ($contact !== FALSE) {
                foreach ($contact as $row) {
                    // start sub branch
                    if($row['phone'] && $row['phone'] != NULL){
                        $phone = $row['phone'];
                    }else{
                        $phone = '-';
                    }
                    $data_r = array(
                        'company_name' => $row['company_name'],
                        'lat' => $row['google_map_lat'],
                        'lng' => $row['google_map_lng'],
                        'email' => $row['email'],
                        'phone' => $phone,
                        'mobile' => $row['mobile'],
                        'address' => $row['address'],
                        'city' => $row['city'],
                        'postcode' => $row['postcode'],
                        'url' => $row['website'],
                    );
                    $xml->startBranch('marker', $data_r);
                    $xml->endBranch();
                }
            }
            // Print the XML to screen
            $getXML = $xml->getXml();
            if($config->pagecache_time == 0){
                $cache_time = 1;
            }else{
                $cache_time = $config->pagecache_time;
            }
            $this->cache->save('contactsdb_getWidget_'.$this->uri->segment(4), $getXML, ($cache_time * 60));
        }
        header('Content-type: text/xml');
        print $this->cache->get('contactsdb_getWidget_'.$this->uri->segment(4));
        exit(0);
    }
    
    public function chkNewsletterIMG() {
        $NID = $this->input->get('NID', TRUE);       
        if($NID){
            $this->db->set('read_n', 'read_n+1', FALSE);
            $this->db->where('contactsdb_newslettersent_id', $NID);
            $this->db->update('contactsdb_newslettersent');
        }
        /* Transparent 1x1 PNG */
        header('Content-Type: image/png');
        echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
        exit(0);
    }
    
    public function unsubscribe() {
        $NID = $this->input->get('NID', TRUE);       
        if($NID){
            $getEmail = $this->Csz_model->getValue('email', 'contactsdb_newslettersent', 'contactsdb_newslettersent_id', $NID, 1);
            if($getEmail !== FALSE){
                $getContactID = $this->Csz_model->getID('contactsdb_data', 'contactsdb_data_id', "email = '".$getEmail->email."'");
                if($getContactID){
                    $this->db->set('unsubscribe', '1');
                    $this->db->where('contactsdb_data_id', $getContactID);
                    $this->db->update('contactsdb_data');
                    $this->session->set_flashdata('f_error_message','<div class="alert alert-success text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your email has been unsubscribed!</div>');
                }else{
                    $this->session->set_flashdata('f_error_message','<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your Email is not have in system!</div>');
                }
            }else{
                $this->session->set_flashdata('f_error_message','<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your Email is not have in system!</div>');
            }
        }
        redirect(BASE_URL, 'refresh');
        exit(0);
    }
    
    public function runnewsletter() {
        /* Run with cronjob 
         * wget http://You_Base_URL/plugin/contactsdb/runnewsletter?pkey= >/dev/null 2>&1
         */
        Key_helper::chkPrivateKey(); /* For check the private key from get parameter ?pkey= */
        $limit = 10; /* send mail limit */
        $this->db->where("date_send <= NOW()", '', FALSE); /* For sql date condition */
        $newsletter = $this->Csz_model->getValue('*', 'contactsdb_newsletter', "email_approve = '1' AND newsletter_complete = '0'", '', 1, 'contactsdb_newsletter_id', 'ASC');        
        if($newsletter !== FALSE){
            $found_email = 0;
            $news_send = $this->Csz_model->getValueArray('*', 'contactsdb_newslettersent', "contactsdb_newsletter_id = '".$newsletter->contactsdb_newsletter_id."' AND sent_n = '0' AND email != ''", '', $limit, 'contactsdb_newslettersent_id', 'ASC', 'email');
            if($news_send !== FALSE){
                foreach ($news_send as $value) {
                    $found_email = 1;
                    $email = $value['email'];
                    if($email){
                        $TableHeader = "<table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>";
                        $TableFooter = "</td></tr><tr><td align=\"center\"><a href=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/unsubscribe?NID=".$value['contactsdb_newslettersent_id']."\">Unsubscribe</a></td></tr></table>";
                        $ChkImg = "<div><img src=\"" . $this->Csz_model->base_link() . "/plugin/contactsdb/chkNewsletterIMG?NID=".$value['contactsdb_newslettersent_id']."\" width=\"1\" height=\"1\"></div>";
                        $this->db->set('newsletter_sent', 'newsletter_sent+1', FALSE);
                        $this->db->where('contactsdb_newsletter_id', $newsletter->contactsdb_newsletter_id);
                        $this->db->update('contactsdb_newsletter');
                        $this->db->set('sent_n', 'sent_n+1', FALSE);
                        $this->db->where('contactsdb_newslettersent_id', $value['contactsdb_newslettersent_id']);
                        $this->db->update('contactsdb_newslettersent');
                        $message_html = $TableHeader.$ChkImg.$newsletter->email_message.$TableFooter;
                        $this->Csz_model->sendEmail($email, $newsletter->email_subject, $message_html, $newsletter->email_from, $newsletter->email_from, '', $newsletter->email_reply, '', '', FALSE);
                    }
                }
            }
            if(!$found_email){
                $this->db->set('newsletter_complete', 1);
                $this->db->set('timestamp_complete', $this->Csz_model->timeNow(), TRUE);
                $this->db->where('contactsdb_newsletter_id', $newsletter->contactsdb_newsletter_id);
                $this->db->update('contactsdb_newsletter');
            }
        }
        echo 'Done.';
        exit(0);
    }
    
    public function runreminder() {
        /* This will run every 5 minutes. Run with cronjob 
         * wget http://You_Base_URL/plugin/contactsdb/runreminder?pkey= >/dev/null 2>&1
         */
        Key_helper::chkPrivateKey(); /* For check the private key from get parameter ?pkey= */
        $limit = 10; /* send mail limit */
        $this->db->where("followup_date = CURDATE()", '', FALSE); /* For sql date condition */
        $reminder = $this->Csz_model->getValueArray('*', 'contactsdb_data', "followup_date != '' AND followup_date != '0000-00-00'", '', 0, 'followup_date', 'ASC');        
        if($reminder !== FALSE){
            $config = $this->Csz_model->load_config();
            $this->lang->load('plugin/contactsdb', $config->admin_lang);
            foreach ($reminder as $row) {
                # ---- send email - renew
                if(!$row['contact_person'] || $row['contact_person'] == NULL) $subject_name = $row['email'];
                else $subject_name = $row['contact_person'].' ['.$row['email'].']';
                $subject = $this->lang->line('contactsdb_reminder_pleasecontact') . " (".$subject_name.")";
                # ---- set from, to, bcc
                $from_name = $config->site_name;
                $from_email = 'no-reply@' . EMAIL_DOMAIN;
                $to_name = $config->site_name;
                $to_email = $config->default_email;
                ($row['phone']) ? $phone = $row['phone'] : $phone = "-";
                ($row['mobile']) ? $mobile = $row['mobile'] : $mobile = "-";
                ($row['website']) ? $website = $row['website'] : $website = "-";
                $message_html = $this->lang->line('contactsdb_reminder_pleasecontact')." " . $to_name . ",<br><br>";
                $message_html .= "<b>".$this->lang->line('contactsdb_reminder_dear')."</b><br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_company_name').":</b> " . $row['company_name'] . "<br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_person').":</b> " . $row['contact_person'] . "<br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_email').":</b> " . $row['email'] . "<br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_phone').":</b> " . $phone . "<br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_mobile').":</b> " . $mobile . "<br>";
                $message_html .= "<b>- ".$this->lang->line('contactsdb_website').":</b> " . $website . "<br>";
                $message_html .= $this->lang->line('contactsdb_reminder_edit')." <a href=\"" . $this->Csz_model->base_link() . "/admin/plugin/contactsdb/contactEdit/" . $row['contactsdb_data_id'] . "\" target=\"_blank\">" . $this->Csz_model->base_link() . "/admin/plugin/contactsdb/contactEdit/" . $row['contactsdb_data_id'] . "</a><br><br>";
                $message_html .= $this->lang->line('contactsdb_reminder_regards')."<br>";
                $message_html .= '<a href="'.$this->Csz_model->base_link().'" target="_blank">'.$config->site_name.'</a>';
                @$this->Csz_model->sendEmail($to_email, $subject, $message_html, $from_email, $from_name, '', $row['email'], '', '', FALSE);
            }
        }
        echo 'Done.';
        exit(0);
    }

}
