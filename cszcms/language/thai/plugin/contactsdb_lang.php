<?php
//Contact DB Index
$lang['contactsdb_header']                    = "ฐานข้อมูลการติดต่อ";
$lang['contactsdb_menu']                      = "แดชบอร์ด";
$lang['contactsdb_newsletter']                      = "จดหมายข่าว";

//Contact DB Index
$lang['contactsdb_addnew']             = "เพิ่มฐานข้อมูลการติดต่อ";
$lang['contactsdb_edit']             = "แก้ไขฐานข้อมูลการติดต่อ";

//Contact DB New
$lang['contactsdb_company_name']             = "ชื่อบริษัท";
$lang['contactsdb_gmap_xml']             = "Google Map XML Url";
$lang['contactsdb_gmap_code']             = "Google Map Code";
$lang['contactsdb_gmap_remark']             = "{type_id} คือไอดีประเภทของการติดต่อ. คุณสามารถดูได้ที่ 'ประเภทของการติดต่อ'. ถ้าหากคุณต้องการดึงข้อมูลทั้งหมดโปรดข้ามสิ่งนี้.";
$lang['contactsdb_gmap_getcode']             = "แสดงโค๊ด";
$lang['contactsdb_gmap_config_remark']             = "กำหนดค่าคีย์ Google Map API ใน 'การตั้งค่าไซต์' ก่อนใช้งาน และใช้โค้ด Google Map ด้านล่างนี้ใน 'เนื้อหาของหน้าเว็บ'";
$lang['contactsdb_type']             = "ประเภทของการติดต่อ";
$lang['contactsdb_person']             = "บุคคล";
$lang['contactsdb_website']             = "เว็บไซต์";
$lang['contactsdb_position']             = "ตำแหน่ง";
$lang['contactsdb_email']             = "อีเมล์";
$lang['contactsdb_phone']             = "โทรศัพท์ (สายภาคพื้นดิน)";
$lang['contactsdb_mobile']             = "มือถือ";
$lang['contactsdb_address']             = "ที่อยู่";
$lang['contactsdb_city']             = "เมือง";
$lang['contactsdb_postcode']             = "รหัสไปรษณีย์";
$lang['contactsdb_contact_note']             = "บันทึกการติดต่อ";
$lang['contactsdb_followup_date']             = "วันที่ในการติดตาม";
$lang['contactsdb_active']             = "เปิดใช้งาน";
$lang['contactsdb_nonactive']             = "ไม่เปิดใช้งาน";
$lang['contactsdb_upload_file']             = "ไฟล์อัพโหลด";
$lang['contactsdb_map_enable']             = "เปิดใช้งานแผนที่";
$lang['contactsdb_map_search_box']             = "กล่องค้นหา";
$lang['contactsdb_map_drag_cursor']             = "กรุณาเลื่อนจุดแดงไปยังสถานที่ตั้ง";
$lang['contactsdb_map_reset']             = "ล้างค่าแผนที่";
$lang['contactsdb_subscribe']             = "สมัครจดหมายข่าว";
$lang['contactsdb_unsubscribe']             = "ยกเลิกจดหมายข่ว";
$lang['contactsdb_last_newsletter']             = "จดหมายข่าวล่าสุด";

//Contact Type
$lang['contactsdb_type_addnew']             = "เพิ่มประเภทการติดต่อ";
$lang['contactsdb_type_edit']             = "แก้ไขประเภทการติดต่อ";
$lang['contactsdb_type_name']             = "ชื่อของประเภท";

//Newsletter
$lang['contactsdb_newsletter_addnew']             = "สร้างจดหมายข่าว";
$lang['contactsdb_newsletter_edit']             = "แก้ไขจดหมายข่าว";
$lang['contactsdb_newsletter_from']             = "จากอีเมล์";
$lang['contactsdb_newsletter_reply']             = "ตอบกลับอีเมล์";
$lang['contactsdb_newsletter_subject']             = "หัวข้อ";
$lang['contactsdb_newsletter_message']             = "เนื้อหาข้อความ";
$lang['contactsdb_newsletter_sentto']             = "เลือกสำหรับการสงไปยัง";
$lang['contactsdb_newsletter_sendall_remark']             = "กรุณาเว้นว่างเอาไว้ เพื่อส่งไปยังรายชื่ออีเมลทั้งหมด";
$lang['contactsdb_newsletter_select_contact']             = "เลือกชื่อบริษัท";
$lang['contactsdb_newsletter_contactsdb_type_id']             = "หรือ เลือกประเภทของการติดต่อ";
$lang['contactsdb_newsletter_email_test']             = "ทดสอบการส่งอีเมล์";
$lang['contactsdb_newsletter_email_test_remark']             = "ตัวเลือกนี้ไม่ต้องการการอนุมัติ เช่น mail1@email.com, mail2@email.com";
$lang['contactsdb_newsletter_approve']             = "อนุมัติ";
$lang['contactsdb_newsletter_approve_remark']             = "เฉพาะเมื่อคุณพร้อมที่จะส่งเท่านั้น";
$lang['contactsdb_newsletter_date_send']             = "วันที่ส่ง";
$lang['contactsdb_newsletter_date_send_remark']             = "ไม่ต้องใส่วันที่หากต้องการส่งทันที";
$lang['contactsdb_newsletter_complete']             = "เสร็จแล้ว";
$lang['contactsdb_newsletter_send']             = "ส่ง";
$lang['contactsdb_newsletter_read']             = "อ่าน";
$lang['contactsdb_newsletter_complete_date']             = "วันที่เสร็จ";
$lang['contactsdb_newsletter_period']             = "ระยะเวลา";
$lang['contactsdb_newsletter_count_send']             = "จำนวนที่ส่ง";
$lang['contactsdb_newsletter_count_read']             = "จำนวนที่อ่าน";
$lang['contactsdb_newsletter_view_all']             = "ดูทั้งหมด";
$lang['contactsdb_newsletter_view_read']             = "ดูที่อ่าน";
$lang['contactsdb_newsletter_view_unsub']             = "ดูที่ยกเลิกจดหมายข่าว";
$lang['contactsdb_newsletter_double_send']             = "ดูการส่งซ้ำ";
$lang['contactsdb_newsletter_add_unread']             = "สร้างจดหมายข่าวด้วยอีเมล์ที่ไม่อ่าน";
$lang['contactsdb_newsletter_ext_sel']             = "ทั้งหมดจากภายนอกที่เลือก (ฐานข้อมูลการติดต่อ).";
$lang['contactsdb_newsletter_cronjob_command']             = "คำสั่ง Cronjob";
$lang['contactsdb_newsletter_cronjob_remark']             = "โปรดคัดลอกคำสั่งนี้ไปยัง cronjob ในแผงควบคุมโฮสติ้งของคุณ";

//Reminder Email
$lang['contactsdb_reminder_pleasecontact']             = "กรุณาติดต่อ";
$lang['contactsdb_reminder_dear']             = "เรียน";
$lang['contactsdb_reminder_edit']             = "หรือแก้ไขได้ที่";
$lang['contactsdb_reminder_regards']             = "ด้วยความนับถือ,";
