<?php
//Contact DB Index
$lang['contactsdb_header']                    = "Contacts Database";
$lang['contactsdb_menu']                      = "Dashboard";
$lang['contactsdb_newsletter']                      = "Newsletter";

//Contact DB Index
$lang['contactsdb_addnew']             = "Add Contacts DB";
$lang['contactsdb_edit']             = "Edit Contacts DB";

//Contact DB New
$lang['contactsdb_company_name']             = "Company Name";
$lang['contactsdb_gmap_xml']             = "Google Map XML Url";
$lang['contactsdb_gmap_code']             = "Google Map Code";
$lang['contactsdb_gmap_remark']             = "{type_id} is the contacts type id. You can see in 'Contacts Type'. If you want to get all data please ignore this.";
$lang['contactsdb_gmap_getcode']             = "Get Code";
$lang['contactsdb_gmap_config_remark']             = "Please configure the Google Map API key on 'Site Settings' before use. And use Google Map code below this on 'Pages Content'";
$lang['contactsdb_type']             = "Contacts Type";
$lang['contactsdb_person']             = "Person";
$lang['contactsdb_website']             = "Website";
$lang['contactsdb_position']             = "Position";
$lang['contactsdb_email']             = "Email";
$lang['contactsdb_phone']             = "Phone (landline)";
$lang['contactsdb_mobile']             = "Mobile";
$lang['contactsdb_address']             = "Address";
$lang['contactsdb_city']             = "City";
$lang['contactsdb_postcode']             = "Postcode";
$lang['contactsdb_contact_note']             = "Contact Notes";
$lang['contactsdb_followup_date']             = "Follow up date";
$lang['contactsdb_active']             = "Active";
$lang['contactsdb_nonactive']             = "Non-active";
$lang['contactsdb_upload_file']             = "Upload file";
$lang['contactsdb_map_enable']             = "Location Map Enable";
$lang['contactsdb_map_search_box']             = "Search Box";
$lang['contactsdb_map_drag_cursor']             = "Please drag the cursor to the location";
$lang['contactsdb_map_reset']             = "Map Reset";
$lang['contactsdb_subscribe']             = "Subscribe";
$lang['contactsdb_unsubscribe']             = "Unsubscribe";
$lang['contactsdb_last_newsletter']             = "Last Newsletter";

//Contact Type
$lang['contactsdb_type_addnew']             = "Add Contacts Type";
$lang['contactsdb_type_edit']             = "Edit Contacts Type";
$lang['contactsdb_type_name']             = "Type Name";

//Newsletter
$lang['contactsdb_newsletter_addnew']             = "Create Newsletter";
$lang['contactsdb_newsletter_edit']             = "Edit Newsletter";
$lang['contactsdb_newsletter_from']             = "From Email";
$lang['contactsdb_newsletter_reply']             = "Reply Email";
$lang['contactsdb_newsletter_subject']             = "Subject";
$lang['contactsdb_newsletter_message']             = "Message Content";
$lang['contactsdb_newsletter_sentto']             = "Choose for send to";
$lang['contactsdb_newsletter_sendall_remark']             = "Please empty these options below for send to all email list.";
$lang['contactsdb_newsletter_select_contact']             = "Choose Company Name";
$lang['contactsdb_newsletter_contactsdb_type_id']             = "Or choose Contacts Type";
$lang['contactsdb_newsletter_email_test']             = "Test Send Email";
$lang['contactsdb_newsletter_email_test_remark']             = "This option don't need approve. Ex mail1@email.com, mail2@email.com";
$lang['contactsdb_newsletter_approve']             = "Approve";
$lang['contactsdb_newsletter_approve_remark']             = "Only when you ready to send";
$lang['contactsdb_newsletter_date_send']             = "Send Date";
$lang['contactsdb_newsletter_date_send_remark']             = "Ignore date if you want to send immediately";
$lang['contactsdb_newsletter_complete']             = "Complete";
$lang['contactsdb_newsletter_send']             = "Send";
$lang['contactsdb_newsletter_read']             = "Read";
$lang['contactsdb_newsletter_complete_date']             = "Complete Date";
$lang['contactsdb_newsletter_period']             = "Period";
$lang['contactsdb_newsletter_count_send']             = "Count Sending";
$lang['contactsdb_newsletter_count_read']             = "Count Reader";
$lang['contactsdb_newsletter_view_all']             = "View All";
$lang['contactsdb_newsletter_view_read']             = "View Readers";
$lang['contactsdb_newsletter_view_unsub']             = "View Unsubscribe";
$lang['contactsdb_newsletter_double_send']             = "Double Sending";
$lang['contactsdb_newsletter_add_unread']             = "Add Newsletter with Unread";
$lang['contactsdb_newsletter_ext_sel']             = "All from external selected (Contacts Database).";
$lang['contactsdb_newsletter_cronjob_command']             = "Cronjob Command";
$lang['contactsdb_newsletter_cronjob_remark']             = "Please copy this command to cronjob on your hosting control panel.";

//Reminder Email
$lang['contactsdb_reminder_pleasecontact']             = "Please contact";
$lang['contactsdb_reminder_dear']             = "Dear";
$lang['contactsdb_reminder_edit']             = "Or edit at";
$lang['contactsdb_reminder_regards']             = "Regards,";
