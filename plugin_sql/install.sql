DELETE FROM `plugin_manager` WHERE `plugin_config_filename` = 'contactsdb';
INSERT INTO `plugin_manager` (`plugin_manager_id`, `plugin_config_filename`, `plugin_active`, `timestamp_create`, `timestamp_update`) VALUES
('', 'contactsdb', 0, NOW(), NOW());
DELETE FROM `user_perms` WHERE `name` = 'Contacts DB';
DELETE FROM `user_perms` WHERE `name` = 'Contacts DB Newsletter';
INSERT INTO `user_perms` (`user_perms_id`, `name`, `definition`, `permstype`) VALUES
('', 'Contacts DB', 'For shop plugin access permission on backend', 'backend'),
('', 'Contacts DB Newsletter', 'For contact db plugin newsletter access permission on backend', 'backend');
DROP TABLE IF EXISTS `contactsdb_type`;
CREATE TABLE IF NOT EXISTS `contactsdb_type` (
  `contactsdb_type_id` int(11) AUTO_INCREMENT,
  `type_name` varchar(255),
  `active` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`contactsdb_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;
INSERT IGNORE INTO `contactsdb_type` (`contactsdb_type_id`, `type_name`, `active`, `timestamp_create`, `timestamp_update`) VALUES
(1, 'Branch', 1, NOW(), NOW()),
(2, 'Customer', 1, NOW(), NOW()),
(3, 'Reseller', 1, NOW(), NOW()),
(4, 'Other', 1, NOW(), NOW());
DROP TABLE IF EXISTS `contactsdb_data`;
CREATE TABLE IF NOT EXISTS `contactsdb_data` (
  `contactsdb_data_id` int(11) AUTO_INCREMENT,
  `contactsdb_type_id` int(11),
  `company_name` varchar(255),
  `contact_person` varchar(255),
  `person_position` varchar(255),
  `email` varchar(255),
  `phone` varchar(100),
  `mobile` varchar(100),
  `address` text,
  `city` varchar(255),
  `postcode` varchar(10),
  `website` varchar(255),
  `active` int(11),
  `contact_note` text,
  `followup_date` date,
  `upload_file` varchar(255),
  `google_map_active` int(11),
  `google_map_lat` varchar(255),
  `google_map_lng` varchar(255),
  `unsubscribe` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`contactsdb_data_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `contactsdb_newsletter`;
CREATE TABLE IF NOT EXISTS `contactsdb_newsletter` (
  `contactsdb_newsletter_id` int(11) AUTO_INCREMENT,
  `email_from` varchar(255),
  `email_reply` varchar(255),
  `email_subject` varchar(255),
  `email_message` text,
  `email_approve` int(11),
  `external_include` int(11),
  `select_contact` text,
  `contactsdb_type_id` int(11),
  `active` int(11),
  `non_active` int(11),
  `newsletter_sent` int(11),
  `newsletter_complete` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  `timestamp_complete` datetime,
  `emails_count` int(11),
  `date_send` date,
  PRIMARY KEY (`contactsdb_newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `contactsdb_newslettersent`;
CREATE TABLE IF NOT EXISTS `contactsdb_newslettersent` (
  `contactsdb_newslettersent_id` int(11) AUTO_INCREMENT,
  `contactsdb_newsletter_id` int(11),
  `email` varchar(255),
  `sent_n` int(11),
  `read_n` int(11),
  PRIMARY KEY (`contactsdb_newslettersent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;